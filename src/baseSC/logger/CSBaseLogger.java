package baseSC.logger;

import config.ConfigEnvironment;
import logger.main.Logger;
import logger.main.prefixs.ThreadPrefix;
import logger.main.prefixs.TimePrefix;

public class CSBaseLogger extends Logger{
	
	private static final boolean debug = Boolean.parseBoolean(ConfigEnvironment.getProperty("Status.SCBase.Debug"));

	private static final CSBaseLogger logger = new CSBaseLogger();
	
	private CSBaseLogger() {
		super("CSBase", new TimePrefix("dd.MM.yy"), new TimePrefix("hh:mm:ss"), new ThreadPrefix());
	}
	
	public static void debug(String text) {
		if(!debug) return;
		logger.println("[debug] " + text);
	}
	
	public static void debug(String text, long clientId) {
		if(!debug) return;
		logger.println("[debug] " + "[" + clientId + "] " + text);
	}
	
	public static void info(String text) {
		logger.println("[info] " + text);
	}
	
	public static void info(String text, long clientId) {
		logger.println("[info] " + "[" + clientId + "] " + text);
	}

}
