package baseSC.test;

import java.util.ArrayList;

import baseSC.data.events.server.SubscribedMessageEvent;
import baseSC.data.events.server.ToServerMessageEvent;
import baseSC.data.events.server.ToServerMessageEventListener;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.subscriber.SubscribedStatus;
import baseSC.server.ServerManager;
import collection.sync.SyncManager;

public class SCTest implements ToServerMessageEventListener {

	private static final String LOCAL_HOST = "localhost";
	private static final int PORT = 12345;

	public static void main(String[] args) {
		new SCTest();
	}

	private ClientTest ct1;
	private ClientTest ct2;

	private ServerManager sm;

	private final SyncManager syncManager = new SyncManager();

	public SCTest() {
		sm = new ServerManager(PORT);
		sm.getDataPackageManager().createPackageType(SingleDTO.class);
		sm.getDataPackageManager().createPackageType(MessageDTO.class);
		sm.getDataPackageManager().createPackageType(MessageHandlerDTO.class);
		sm.getEventManager().registerServerMessageEventListener(this, 0);
		sm.openConnection();

		syncManager.asyncronizedWait(100);

		ct1 = new ClientTest(1, LOCAL_HOST, PORT);
		ct2 = new ClientTest(2, LOCAL_HOST, PORT);

		new Thread(() -> {
			while (true) {
				syncManager.asyncronizedWait(1);

				ct1.tick();
				ct2.tick();

				// for (long clientId : sm.getConnectedClients()) {
					// System.out.println("To-Client-Ping (" + clientId + ") : " +
					// sm.getPing(clientId));
				// }

				sm.getEventManager().tick();
			}
		}).start();

		MessageDTO messageDTO = new MessageDTO("Test1", "Test2", "Test3");
		messageDTO.setMessages(new ArrayList<SingleDTO>());
		
		ct1.sendMessage(messageDTO);
		System.out.println(ct1.sendSubscribedMessage(messageDTO));

		messageDTO.getMessages().add(new SingleDTO("Test-Test-Test"));
		messageDTO.getMessages().add(new SingleDTO("Test2-Test2-Test2"));
		messageDTO.getMessages().add(new SingleDTO(null));

		ct1.sendMessage(messageDTO);
		System.out.println(ct1.sendSubscribedMessage(messageDTO));

		ct1.sendMessage(new MessageDTO("Test1", "Test2", null));
		ct2.sendMessage(new MessageDTO("Test4", "Test5", "Test6"));

		sm.sendMessage(1, new MessageDTO("Test7", "Test8", "Test9"));
		String veryLongMsg = "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111119";
		sm.sendMessage(2, new MessageDTO("veryLongMsg", veryLongMsg, veryLongMsg));

		ct1.sendMessage(new MessageHandlerDTO(1, 2, new MessageDTO("Test10", "Test11", "Test12")));
	}

	@Override
	public void messageFromClient(ToServerMessageEvent event) {
		if (event.getObjectDTO() instanceof MessageDTO) {
			MessageDTO message = (MessageDTO) event.getObjectDTO();
			System.out.println("New message from client " + event.getClientID() + " : " + message);
		} else if (event.getObjectDTO() instanceof MessageHandlerDTO) {
			MessageHandlerDTO message = (MessageHandlerDTO) event.getObjectDTO();
			System.out.println("New message from client " + event.getClientID() + " : " + "(" + message.getId() + ", "
					+ message.getSenderId() + ") -> " + message.getMessage().getMessage1() + ", "
					+ message.getMessage().getMessage2() + ", " + message.getMessage().getMessage3());

		}
		event.setActive(false);
	}

	@Override
	public void messageFromClient(SubscribedMessageEvent event) {
		System.out.println("New message from client " + event.getClientID() + " : " + event.getRequest());
		event.setAnswer(new SubscribedMessageDTO(event.getRequest().getSmHeader(), event.getRequest().getContent()));
		event.getAnswer().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
		event.setActive(false);
	}
}
