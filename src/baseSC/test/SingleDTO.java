package baseSC.test;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.SingleDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.SCBaseTest.Single")
public class SingleDTO implements DTO {

	public SingleDTO() {
		super();
	}

	public SingleDTO(String message1) {
		super();
		this.message1 = message1;
	}

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1)
	private String message1;

	@Override
	public DTO copy() {
		return new SingleDTO(message1);
	}

	public String getMessage1() {
		return message1;
	}

	@Override
	public String toString() {
		return "SingleDTO [message1=" + message1 + "]";
	}

}
