package baseSC.test;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;
import baseSC.data.types.SpecialDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.SCBaseTest.MessageHandler")
public class MessageHandlerDTO implements DTO{

	public MessageHandlerDTO(){};
	
	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_LONG, defaultValueId = DefaultValue.ID_LONG_0)
	private long senderId;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_LONG, defaultValueId = DefaultValue.ID_LONG_0)
	private long id;

	@PackagableContent(contentTypeKey = SpecialDataReaderType.KEY_SUBENTITY)
	private MessageDTO message;

	public MessageHandlerDTO(long senderId, long id, MessageDTO message) {
		super();
		this.senderId = senderId;
		this.id = id;
		this.message = message;
	}

	public long getSenderId() {
		return senderId;
	}

	public long getId() {
		return id;
	}

	public MessageDTO getMessage() {
		return message;
	}

	@Override
	public DTO copy() {
		return new MessageHandlerDTO(senderId, id, (MessageDTO) message.copy());
	}
}
