package baseSC.test;

import baseSC.client.ClientManager;
import baseSC.data.DTO;
import baseSC.data.events.client.ToClientMessageEvent;
import baseSC.data.events.client.ToClientMessageEventListener;
import baseSC.data.internelDTO.SubscribedMessageDTO;

public class ClientTest implements ToClientMessageEventListener{

	private ClientManager cm;
	private int id;
	
	public ClientTest(int id, String ip, int port) {
		this.id = id;
		cm = new ClientManager(ip, port);
		cm.connectToServer();
		cm.getEventManager().registerClientMessageEventListener(this, 0);
		cm.getPackageManager().createPackageType(SingleDTO.class);
		cm.getPackageManager().createPackageType(MessageDTO.class);
		cm.getPackageManager().createPackageType(MessageHandlerDTO.class);
	}

	public ClientTest(int id, String ip, int port, String keyStorePath) {
		this.id = id;
		cm = new ClientManager(ip, port, keyStorePath);
		cm.connectToServer();
		cm.getEventManager().registerClientMessageEventListener(this, 0);
		cm.getPackageManager().createPackageType(SingleDTO.class);
		cm.getPackageManager().createPackageType(MessageDTO.class);
		cm.getPackageManager().createPackageType(MessageHandlerDTO.class);
	}

	public void tick() {
//		System.out.println("To-Server-Ping (" + id + ") : " + cm.getPing());
		
		cm.getEventManager().tick();
	}

	@Override
	public void messageFromServer(ToClientMessageEvent event) {
		if (event.getObjectDTO() instanceof MessageDTO) {
			MessageDTO message = (MessageDTO) event.getObjectDTO();
			System.out.println("New Message to client " + id + " : " + message.getMessage1() + ", " + message.getMessage2() + ", " + message.getMessage3());
		}
		event.setActive(false);
	}

	public void sendMessage(DTO objDTO) {
		this.cm.sendMessage(objDTO);
	}

	public SubscribedMessageDTO sendSubscribedMessage(DTO objDTO) {
		return cm.subscribe(objDTO, 10000);
	}

}
