package baseSC.test;

import java.util.ArrayList;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.MultiDataReaderType;
import baseSC.data.types.SingleDataReaderType;
import baseSC.data.types.SpecialDataReaderType;
import collection.Utills;

@PackagableEntity(configKey = "Connection.DTO-Ids.SCBaseTest.Message")
public class MessageDTO implements DTO {

	public MessageDTO() {
	};

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1)
	private String message1;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1)
	private String message2;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1)
	private String message3;

	@PackagableContent(dataTypeKey = MultiDataReaderType.KEY_ARRAY_LIST, contentTypeKey = SpecialDataReaderType.KEY_SUBENTITY)
	private ArrayList<SingleDTO> messages;

	public MessageDTO(String message1, String message2, String message3) {
		super();
		this.message1 = message1;
		this.message2 = message2;
		this.message3 = message3;
	}

	public String getMessage1() {
		return message1;
	}

	public String getMessage2() {
		return message2;
	}

	public String getMessage3() {
		return message3;
	}

	@Override
	public DTO copy() {
		MessageDTO message = new MessageDTO(message1, message2, message3);
		message.setMessages(Utills.cloneArrayList(messages));
		return message;
	}

	@Override
	public String toString() {
		return "MessageDTO [message1=" + message1 + ", message2=" + message2 + ", message3=" + message3 + ", messages="
				+ messages + "]";
	}

	public ArrayList<SingleDTO> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<SingleDTO> messages) {
		this.messages = messages;
	}

}
