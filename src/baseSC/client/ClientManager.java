package baseSC.client;

import baseSC.data.DTO;
import baseSC.data.exceptions.client.ServerNotFoundException;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.packageType.PackageManager;

public class ClientManager {

	private Client client;

	public ClientManager(String ip, int port) {
		this.client = new Client(ip, port);
	}

	public ClientManager(String ip, int port, String keyStorePath) {
		this.client = new SSLClient(ip, port, keyStorePath);
	}

	public ClientEventManager getEventManager() {
		return this.client.getEventManager();
	}

	public void connectToServer() throws ServerNotFoundException {
		this.client.connect();
	}

	public void sendMessage(DTO objectDTO) {
		client.getClientTaskManager().sendMessage(objectDTO.copy());
	}

	public void endClient() {
		this.client.endClient();
	}

	public boolean isConnected() {
		return client.isConnected();
	}

	public PackageManager getPackageManager() {
		return client.getPackageManager();
	}

	public long getPing() {
		return client.getPing();
	}
	
	public SubscribedMessageDTO subscribe(DTO message, long validDuration) {
		return client.getMessageSubscriber().subscribe(message.copy(), validDuration);
	}

}
