package baseSC.client;

import java.io.File;

import config.ConfigEnvironment;

public class SSLClient extends Client {

	private File keyStore;

	protected SSLClient(String ip, int port, String keyStorePath) {
		super(ip, port);
		this.keyStore = new File(ConfigEnvironment.getProperty(keyStorePath));
		System.setProperty("javax.net.ssl.trustStore", keyStore.getAbsolutePath());
	}

}
