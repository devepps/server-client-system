package baseSC.client;

import baseSC.data.DTO;
import baseSC.data.internelDTO.HeartBeatDTO;
import baseSC.logger.CSBaseLogger;
import collection.sync.SyncQueue;
import data.queue.Queue;

public class ClientTaskManager {

	private final SyncQueue<DTO> messages = new SyncQueue<>();

	private Client client;
	private int lastPackageId = 0;

	public ClientTaskManager(Client client) {
		super();
		this.client = client;
	}

	public void tick() {
		Queue<DTO> copyMessages = messages.clearAndClone();

		while (!copyMessages.isEmpty()) {
			Object message = copyMessages.pop();

			if (client.isConnected()) {
				if (!(message instanceof HeartBeatDTO)) {
					CSBaseLogger.debug("Client send: " + message);
				}
				lastPackageId++;
				client.getCCH().send(client.getPackageManager().readDTO(message, lastPackageId));
			}
		}
	}

	public void sendMessage(DTO DTO) {
		messages.add(DTO);
	}
	
	

}
