package baseSC.client;

import baseSC.data.DTO;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.subscriber.SMHeader;
import baseSC.data.subscriber.SubscribedStatus;
import baseSC.logger.CSBaseLogger;
import collection.sync.SyncHashMap;
import collection.sync.SyncedData;

public class MessageSubscriber {

	private final SyncHashMap<Long, SubscribedMessageDTO> subscribedMessages = new SyncHashMap<>();
	private final SyncedData<Long> lastId = new SyncedData<>(-1l);
	
	private Client client;

	public MessageSubscriber(Client client) {
		super();
		this.client = client;
	}

	public SubscribedMessageDTO subscribe(DTO message, long validDuration) {
		SubscribedMessageDTO subMsg = null;
		long id = 0;
		while (subMsg == null) {
			id = lastId.update((Long lastId) -> lastId + 1);
			subMsg = subscribedMessages.get(id);
			if (subMsg == null || System.currentTimeMillis() - subMsg.getSmHeader().getCreated() > subMsg.getSmHeader()
					.getValidDuration()) {
				subMsg = new SubscribedMessageDTO(new SMHeader(id, validDuration, System.currentTimeMillis(),
						SubscribedStatus.PENDING.getId()), message);
				subscribedMessages.put(id, subMsg);
			} else {
				subMsg = null;
			}
		}
		CSBaseLogger.debug("[" + this.hashCode() + "] listen on Id: " + id + " -> " + subscribedMessages.get(id));

		try {
			client.getClientTaskManager().sendMessage(subMsg);
		} catch (Throwable e) {
			e.printStackTrace();
			return exit(SubscribedStatus.CLIENT_ERROR, subMsg);
		}
		CSBaseLogger.debug("[" + this.hashCode() + "] Message (" + id + ") send");

		synchronized (subMsg) {
			try {
				subMsg.wait(validDuration);
				if (System.currentTimeMillis() - subMsg.getSmHeader().getCreated() > validDuration) {
					return exit(SubscribedStatus.EXPIRED, subMsg);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				return exit(SubscribedStatus.CLIENT_ERROR, subMsg);
			}
		}
		CSBaseLogger.debug("[" + this.hashCode() + "] Message (" + id + ") recieved: " + subscribedMessages.get(id));

		SubscribedMessageDTO answer = subscribedMessages.get(id);
		CSBaseLogger.debug("[" + this.hashCode() + "] recieve answer: " + answer);
		subscribedMessages.remove(id);
		return answer;
	}

	private SubscribedMessageDTO exit(SubscribedStatus status, SubscribedMessageDTO subMsg) {
		subscribedMessages.remove(subMsg.getSmHeader().getId());
		subMsg.getSmHeader().setStatus(status.getId());
		return new SubscribedMessageDTO(subMsg.getSmHeader(), null);
	}

	public void recieveSubscribedMessage(SubscribedMessageDTO subMessage) {
		CSBaseLogger.debug("start publishing answer: " + subMessage);
		if (subMessage != null && subMessage.getSmHeader() != null) {
			SubscribedMessageDTO request = subscribedMessages.get(subMessage.getSmHeader().getId());
			CSBaseLogger.debug("request found: " + request);
			if (request != null) {
				CSBaseLogger.debug("publish answer: " + subMessage);
				subscribedMessages.put(subMessage.getSmHeader().getId(), subMessage);
				synchronized (request) {
					request.notifyAll();
				}
			}
		}
	}
}
