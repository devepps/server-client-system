package baseSC.client;

import java.net.InetAddress;
import java.net.UnknownHostException;

import baseSC.data.internelDTO.HeartBeatDTO;
import baseSC.data.packageType.PackageManager;
import collection.sync.SyncManager;

public class Client {

	private final static int maxHeartbeatDuration = 10000;

	private String ip;
	private int port;

	private MessageSubscriber messageSubscriber;
	private ClientConnectionHandler cch;
	private ClientEventManager eventManager;
	private PackageManager packageManager;
	private ClientTaskManager clientTaskManager;

	private boolean run = true;
	private long lastHeartBeat;
	private long ping;

	protected Client(String ip, int port) {
		this.ip = ip;
		this.port = port;

		this.eventManager = new ClientEventManager();
		this.packageManager = new PackageManager();
		this.cch = new ClientConnectionHandler(this);
		this.clientTaskManager = new ClientTaskManager(this);
		this.messageSubscriber = new MessageSubscriber(this);

		try {
			InetAddress Host = InetAddress.getByName(ip);
			ip = Host.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public void connect() {
		this.cch.connect(ip, port);

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (run && isConnected()) {
					cch.scanForIncomingData();
				}
			}
		}).start();

		new Thread(new Runnable() {
			private final SyncManager syncManager = new SyncManager();

			@Override
			public void run() {
				while (run) {
					clientTaskManager.tick();
					syncManager.asyncronizedWait(0, 1);
				}
			}
		}).start();

		new Thread(new Runnable() {
			private final SyncManager syncManager = new SyncManager();

			@Override
			public void run() {
				while (run) {
					if (System.currentTimeMillis() - lastHeartBeat > maxHeartbeatDuration && lastHeartBeat != 0) {
						endClient();
					}
					clientTaskManager.sendMessage(new HeartBeatDTO());
					syncManager.asyncronizedWait(1000);
				}
			}
		}).start();
	}

	void endClient() {
		this.cch.endConnection();
		this.run = false;
	}

	public boolean run() {
		return run;
	}

	boolean isConnected() {
		return cch.isConnected() && run;
	}

	public ClientEventManager getEventManager() {
		return eventManager;
	}

	public PackageManager getPackageManager() {
		return packageManager;
	}

	public ClientConnectionHandler getCCH() {
		return cch;
	}

	public ClientTaskManager getClientTaskManager() {
		return clientTaskManager;
	}

	public void receiveHeartBeat(HeartBeatDTO obj) {
		this.lastHeartBeat = System.currentTimeMillis();
		this.ping = System.currentTimeMillis() - obj.getCurrentTime();
	}

	public long getPing() {
		return ping;
	}

	public MessageSubscriber getMessageSubscriber() {
		return messageSubscriber;
	}
}
