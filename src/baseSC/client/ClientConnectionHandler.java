package baseSC.client;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import baseSC.data.InputStreamHandler;
import baseSC.data.dataPackage.DataQueueIn;
import baseSC.data.events.client.ClientLostConnectionToServerEvent;
import baseSC.data.events.client.ToClientMessageEvent;
import baseSC.data.exceptions.client.ServerNotFoundException;
import baseSC.data.internelDTO.HeartBeatDTO;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.packageType.PackageManager;
import baseSC.logger.CSBaseLogger;
import data.queue.Queue;

public class ClientConnectionHandler {

	private Client client;
	private Socket socket;

	private InputStreamHandler inputHandler;
	private BufferedOutputStream out;

	private boolean ended = false;

	public ClientConnectionHandler(Client client) {
		this.client = client;
	}

	public void connect(String ip, int port) {
		try {
			if (client instanceof SSLClient) {
				SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
				SSLSocket socket = (SSLSocket) factory.createSocket(ip, port);
				this.socket = socket;
			} else {
				socket = new Socket(ip, port);
			}
			socket.setTcpNoDelay(false);
			socket.setReceiveBufferSize(PackageManager.PACKAGE_DATA_SIZE);
			socket.setSendBufferSize(PackageManager.PACKAGE_DATA_SIZE);

			this.out = new BufferedOutputStream(socket.getOutputStream(), PackageManager.PACKAGE_DATA_SIZE);
			this.inputHandler = new InputStreamHandler(new DataInputStream(socket.getInputStream()));
		} catch (IOException e) {
			throw new ServerNotFoundException(e, ip, port);
		}
	}

	public void scanForIncomingData() {
		try {
			while (isConnected()) {
				DataQueueIn dataQueue = inputHandler.readData();
				CSBaseLogger.debug("new Message: " + dataQueue);
				try {
					if (dataQueue != null) {
						CSBaseLogger.debug("start reading Message");
						Object obj = client.getPackageManager().readDataQueue(dataQueue);
						CSBaseLogger.debug("read Message: " + obj);
						if (obj instanceof HeartBeatDTO) {
							client.receiveHeartBeat((HeartBeatDTO) obj);
						} else if (obj instanceof SubscribedMessageDTO) {
							client.getMessageSubscriber().recieveSubscribedMessage((SubscribedMessageDTO) obj);
						} else {
							client.getEventManager().publishClientEvent(new ToClientMessageEvent(obj));
						}
					}
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			if (isConnected()) {
				if (!ended) {
					ended = true;
					this.client.endClient();
					client.getEventManager().publishClientEvent(new ClientLostConnectionToServerEvent(socket));
				}
			}
		}
	}

	public void send(Queue<byte[]> packages) {
		if (isConnected()) {
			try {
				while (!packages.isEmpty()) {
					this.out.write(packages.get());
					packages.remove();
				}
			} catch (IOException e) {
				e.printStackTrace();
				if (this.client.run() && !ended) {
					this.client.endClient();
					this.client.getEventManager().publishClientEvent(new ClientLostConnectionToServerEvent(socket));
				}
				ended = true;
			}
		}
	}

	boolean isConnected() {
		return socket != null && socket.isConnected() && !socket.isClosed() && client.run();
	}

	void endConnection() {
		this.socket = null;
	}

	Socket getConnection() {
		return socket;
	}

	boolean isEnded() {
		return ended;
	}

}
