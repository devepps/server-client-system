package baseSC.data.subscriber;

public class SMHeader {

	private long id;
	private long validDuration;
	private long created;
	private int status;

	public SMHeader(long id, long validDuration, long created, int status) {
		super();
		this.id = id;
		this.validDuration = validDuration;
		this.created = created;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public long getValidDuration() {
		return validDuration;
	}

	public long getCreated() {
		return created;
	}

	public int getStatusId() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SMHeader [id=" + id + ", validDuration=" + validDuration + ", created=" + created + ", status=" + status
				+ "]";
	}
}
