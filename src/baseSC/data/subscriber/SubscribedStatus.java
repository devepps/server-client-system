package baseSC.data.subscriber;

public enum SubscribedStatus {

	PENDING(10000, "Request is currently pending."),
	OK(20000, "Request was successful."),
	CREATED(21000, "Data of request has been succesfuly created."),
	ACCEPTED(22000, "Request has been accpeted. Action will be done later."),
	FAILURE(30000, "Request has failed."),
	NOT_IMPLEMENTED(31000, "The requested action is unknown to the server."),
	CLIENT_ERROR(32000, "The request has failed to be send."),
	BAD_REQUEST(33000, "Request has invalid content."),
	EXPIRED(34000, "Request has expired."),
	UNAUTHERIZED(35000, "You don't have the permission needed for this request."),
	NO_ANSWER(36000, "The request has yielded no result.");

	private int id;
	private String message;

	private SubscribedStatus(int id, String message) {
		this.id = id;
		this.message = message;
	}

	public int getId() {
		return id;
	}

	public String getMessage() {
		return message;
	}

}
