package baseSC.data.events.client;

public class ToClientMessageEvent extends ClientEvent {

	private Object objectDTO;

	public ToClientMessageEvent(Object objectDTO) {
		super();
		this.objectDTO = objectDTO;
	}

	public Object getObjectDTO() {
		return objectDTO;
	}

}
