package baseSC.data.events.server;

public interface ToServerMessageEventListener {

	public void messageFromClient(ToServerMessageEvent event);

	public void messageFromClient(SubscribedMessageEvent event);

}
