package baseSC.data.events.server;

import baseSC.logger.CSBaseLogger;

public class NewClientConnectionEvent extends ServerEvent{

	public NewClientConnectionEvent(long clientID) {
		super(clientID);
		CSBaseLogger.debug("New client connected server: " + clientID);
	}

}
