package baseSC.data.events.server;

public class ToServerMessageEvent extends ServerEvent{

	private Object objectDTO;
	
	public ToServerMessageEvent(long clientID, Object objectDTO) {
		super(clientID);
		this.objectDTO = objectDTO;
	}

	public Object getObjectDTO() {
		return objectDTO;
	}
}
