package baseSC.data.events.server;

import baseSC.data.internelDTO.SubscribedMessageDTO;

public class SubscribedMessageEvent extends ServerEvent {

	private SubscribedMessageDTO request;
	private SubscribedMessageDTO answer;
	private boolean leaveUnAnswered = false;

	public SubscribedMessageEvent(long clientID, SubscribedMessageDTO request) {
		super(clientID);
		this.request = request;
	}

	public SubscribedMessageDTO getRequest() {
		return request;
	}

	public SubscribedMessageDTO getAnswer() {
		return answer;
	}

	public void setAnswer(SubscribedMessageDTO answer) {
		this.answer = answer;
	}

	public void leaveUnAnswered() {
		leaveUnAnswered = true;
	}

	public boolean doLeaveUnAnswered() {
		return leaveUnAnswered;
	}

	@Override
	public String toString() {
		return "SubscribedMessageEvent [request=" + request + ", answer=" + answer + ", leaveUnAnswered=" + leaveUnAnswered + "]";
	}
}
