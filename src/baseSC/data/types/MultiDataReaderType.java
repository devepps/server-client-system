package baseSC.data.types;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import baseSC.data.dataReader.MultiDataReader;
import baseSC.data.dataReader.multiDataReader.ArrayListReader;
import baseSC.data.dataReader.multiDataReader.MultiSingleDataReader;
import baseSC.logger.CSBaseLogger;
import config.ConfigEnvironment;

public class MultiDataReaderType {

	public static final String KEY_SINGLE = "Connection.Data.Reader-ID.Multi.Single:0";
	public static final String KEY_ARRAY_LIST = "Connection.Data.Reader-ID.Multi.ArrayList:0";

	public static final MultiDataReaderType SINGLE = new MultiDataReaderType(KEY_SINGLE, MultiSingleDataReader.class);
	public static final MultiDataReaderType ARRAY_LIST = new MultiDataReaderType(KEY_ARRAY_LIST, ArrayListReader.class);

	private static ArrayList<MultiDataReaderType> DATA_READER_TYPES;

	private Class<? extends MultiDataReader> readerClass;
	private String configKeyFull;
	private int id;

	protected MultiDataReaderType(String configKey, int configKeyOrder, Class<? extends MultiDataReader> readerClass) {
		this.configKeyFull = configKey + ":" + configKeyOrder;
		this.readerClass = readerClass;
		try {
			this.readerClass.getConstructor(SingleDataReaderType.class).setAccessible(true);
		} catch (SecurityException | NoSuchMethodException e) {
			e.printStackTrace();
		}

		this.id = Integer.parseInt(ConfigEnvironment.getProperties(configKey).get(configKeyOrder));

		CSBaseLogger.debug("MultiDataReader: \"" + configKey + "\" : " + configKeyOrder + " has been registered as: " + id);
		if (DATA_READER_TYPES == null) {
			DATA_READER_TYPES = new ArrayList<>();
		}
		DATA_READER_TYPES.add(this);
	}

	public MultiDataReaderType(String configKeyFull, Class<? extends MultiDataReader> readerClass) {
		this.configKeyFull = configKeyFull;
		this.readerClass = readerClass;
		try {
			this.readerClass.getConstructor(DataReaderType.class).setAccessible(true);
		} catch (SecurityException | NoSuchMethodException e) {
			e.printStackTrace();
		}

		String[] configKeyParams = configKeyFull.split(":");
		this.id = Integer.parseInt(	ConfigEnvironment.getProperties(configKeyParams[0]).get(Integer.parseInt(configKeyParams[1])));

		CSBaseLogger.debug("MultiDataReader: \"" + configKeyParams[0] + "\" : " + configKeyParams[1] + " has been registered as: " + id);
		if (DATA_READER_TYPES == null) {
			DATA_READER_TYPES = new ArrayList<>();
		}
		DATA_READER_TYPES.add(this);
	}

	public static MultiDataReaderType fromId(int id) {
		for (int i = 0; i < DATA_READER_TYPES.size(); i++) {
			MultiDataReaderType readerTyp = DATA_READER_TYPES.get(i);
			if (readerTyp.getId() == id) {
				return readerTyp;
			}
		}
		return null;
	}

	public static MultiDataReaderType getType(String configKeyFull) {
		for (int i = 0; i < DATA_READER_TYPES.size(); i++) {
			MultiDataReaderType readerTyp = DATA_READER_TYPES.get(i);
			if (readerTyp.configKeyFull.equals(configKeyFull)) {
				return readerTyp;
			}
		}
		return null;
	}

	public int getId() {
		return id;
	}

	public MultiDataReader create(DataReaderType dataReaderType) {
		try {
			return readerClass.getConstructor(DataReaderType.class).newInstance(dataReaderType);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}
}
