package baseSC.data.types;

import java.util.ArrayList;

import baseSC.data.dataReader.DataReader;
import baseSC.logger.CSBaseLogger;
import config.ConfigEnvironment;

public abstract class DataReaderType {
	
	public static void load() {
		if (DATA_READER_TYPES == null) {
			DATA_READER_TYPES = new ArrayList<>();
			SingleDataReaderType.loadContent();
			SpecialDataReaderType.loadContent();
		}
	}

	private static ArrayList<DataReaderType> DATA_READER_TYPES;

	private int id;
	private String configKeyFull;
	private DataReader reader;

	public DataReaderType(String configKeyFull, DataReader reader) {
		String[] configKeyParams = configKeyFull.split(":");
		this.id = Integer.parseInt(ConfigEnvironment.getProperties(configKeyParams[0]).get(Integer.parseInt(configKeyParams[1])));
		CSBaseLogger.info("DataReaderType: \"" + configKeyParams[0] + "\" : " + configKeyParams[1] + " has been registered as: " + id);
		this.configKeyFull = configKeyFull;
		
		this.reader = reader;
	
		if (DATA_READER_TYPES == null) {
			load();
		}
		DATA_READER_TYPES.add(this);
	}

	public DataReaderType(String configKey, int configKeyOrder, DataReader reader) {
		this.id = Integer.parseInt(ConfigEnvironment.getProperties(configKey).get(configKeyOrder));
		CSBaseLogger.info("DataReaderType: \"" + configKey + "\" : " + configKeyOrder + " has been registered as: " + id);
		this.configKeyFull = configKey + ":" + configKeyOrder;
		
		this.reader = reader;
		
		if (DATA_READER_TYPES == null) {
			load();
		}
		DATA_READER_TYPES.add(this);
	}

	public int getId() {
		return id;
	}

	public DataReader getReader() {
		return reader;
	}

	public static DataReaderType fromId(int id) {
		for (int i = 0; i < DATA_READER_TYPES.size(); i++) {
			DataReaderType readerTyp = DATA_READER_TYPES.get(i);
			if (readerTyp.getId() == id) {
				return readerTyp;
			}
		}
		return null;
	}

	public static DataReaderType getType(String configKeyFull) {
		for (int i = 0; i < DATA_READER_TYPES.size(); i++) {
			DataReaderType readerTyp = DATA_READER_TYPES.get(i);
			if (readerTyp.configKeyFull.equals(configKeyFull)) {
				return readerTyp;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "DataReaderType [id=" + id + ", configKeyFull=" + configKeyFull + ", reader=" + reader + "]";
	}
}
