package baseSC.data.types;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.dataReader.singleDataReader.BooleanReader;
import baseSC.data.dataReader.singleDataReader.ByteArrayReader;
import baseSC.data.dataReader.singleDataReader.ByteReader;
import baseSC.data.dataReader.singleDataReader.DateReader;
import baseSC.data.dataReader.singleDataReader.DateTimeReader;
import baseSC.data.dataReader.singleDataReader.DoubleReader;
import baseSC.data.dataReader.singleDataReader.HeaderReader;
import baseSC.data.dataReader.singleDataReader.IntegerReader;
import baseSC.data.dataReader.singleDataReader.LongReader;
import baseSC.data.dataReader.singleDataReader.NullReader;
import baseSC.data.dataReader.singleDataReader.SMHeaderReader;
import baseSC.data.dataReader.singleDataReader.SerializableReader;
import baseSC.data.dataReader.singleDataReader.StringReader_ISO_8859_1;
import baseSC.data.dataReader.singleDataReader.StringReader_UNICODE;
import baseSC.data.dataReader.singleDataReader.TimeReader;
import baseSC.data.dataReader.singleDataReader.UUIDReader;

public class SingleDataReaderType extends DataReaderType {

	public static void loadContent() {
		
	}

	public static final String KEY_HEADER = "Connection.Data.Reader-ID.Single.Header:0";
	public static final String KEY_INTEGER = "Connection.Data.Reader-ID.Single.Integer:0";
	public static final String KEY_BOOLEAN = "Connection.Data.Reader-ID.Single.Boolean:0";
	public static final String KEY_BYTE_ARRAY = "Connection.Data.Reader-ID.Single.Byte_Array:0";
	public static final String KEY_BYTE = "Connection.Data.Reader-ID.Single.Byte:0";
	public static final String KEY_STRING_ISO_8859_1 = "Connection.Data.Reader-ID.Single.String.ISO_8859_1:0";
	public static final String KEY_STRING_UNICODE = "Connection.Data.Reader-ID.Single.String.Unicode:0";
	public static final String KEY_DOUBLE = "Connection.Data.Reader-ID.Single.Double:0";
	public static final String KEY_NULL = "Connection.Data.Reader-ID.Single.Null:0";
	public static final String KEY_LONG = "Connection.Data.Reader-ID.Single.Long:0";
	public static final String KEY_SUBSCRIBED_MESSAGES_HEADER = "Connection.Data.Reader-ID.Single.Subscribed_Messages_Header:0";
	public static final String KEY_SERIALIZABLE = "Connection.Data.Reader-ID.Single.Serializable:0";
	public static final String Key_DATE_TIME = "Connection.Data.Reader-ID.Single.Date-Time:0";
	public static final String Key_TIME = "Connection.Data.Reader-ID.Single.Time:0";
	public static final String Key_DATE = "Connection.Data.Reader-ID.Single.Date:0";
	public static final String Key_UUID = "Connection.Data.Reader-ID.Single.UUID:0";

	public static final SingleDataReaderType HEADER = new SingleDataReaderType(KEY_HEADER, new HeaderReader());
	public static final SingleDataReaderType INTEGER = new SingleDataReaderType(KEY_INTEGER, new IntegerReader());
	public static final SingleDataReaderType BOOLEAN = new SingleDataReaderType(KEY_BOOLEAN, new BooleanReader());
	public static final SingleDataReaderType BYTE_ARRAY = new SingleDataReaderType(KEY_BYTE_ARRAY, new ByteArrayReader());
	public static final SingleDataReaderType BYTE = new SingleDataReaderType(KEY_BYTE, new ByteReader());
	public static final SingleDataReaderType STRING_ISO_8859_1 = new SingleDataReaderType(KEY_STRING_ISO_8859_1, new StringReader_ISO_8859_1());
	public static final SingleDataReaderType STRING_UNICODE = new SingleDataReaderType(KEY_STRING_UNICODE, new StringReader_UNICODE());
	public static final SingleDataReaderType DOUBLE = new SingleDataReaderType(KEY_DOUBLE, new DoubleReader());
	public static final SingleDataReaderType NULL = new SingleDataReaderType(KEY_NULL, new NullReader());
	public static final SingleDataReaderType LONG = new SingleDataReaderType(KEY_LONG, new LongReader());
	public static final SingleDataReaderType SUBSCRIBED_MESSAGES_HEADER = new SingleDataReaderType(KEY_SUBSCRIBED_MESSAGES_HEADER, new SMHeaderReader());
	public static final SingleDataReaderType SERIALIZABLE = new SingleDataReaderType(KEY_SERIALIZABLE, new SerializableReader());
	public static final SingleDataReaderType DATE_TIME = new SingleDataReaderType(Key_DATE_TIME, new DateTimeReader());
	public static final SingleDataReaderType TIME = new SingleDataReaderType(Key_TIME, new TimeReader());
	public static final SingleDataReaderType DATE = new SingleDataReaderType(Key_DATE, new DateReader());
	public static final SingleDataReaderType UUID = new SingleDataReaderType(Key_UUID, new UUIDReader());

	public SingleDataReaderType(String configKey, int configKeyOrder, SingleDataReader reader) {
		super(configKey, configKeyOrder, reader);
	}

	public SingleDataReaderType(String configKeyFull, SingleDataReader reader) {
		super(configKeyFull, reader);
	}

	public SingleDataReader getSingleDataReader() {
		return (SingleDataReader) getReader();
	}
}
