package baseSC.data.types;

import baseSC.data.dataReader.SpecialDataReader;
import baseSC.data.dataReader.singleDataReader.SubEntityReader;

public class SpecialDataReaderType extends DataReaderType{

	public static void loadContent() {
		
	}

	public static final String KEY_SUBENTITY = "Connection.Data.Reader-ID.Special.SubEntity:0";

	public static final SpecialDataReaderType SUBENTITY = new SpecialDataReaderType(KEY_SUBENTITY, new SubEntityReader());

	protected SpecialDataReaderType(String configKeyFull, SpecialDataReader reader) {
		super(configKeyFull, reader);
	}

	protected SpecialDataReaderType(String configKey, int configKeyOrder, SpecialDataReader reader) {
		super(configKey, configKeyOrder, reader);
	}

	public SpecialDataReader getSpecialDataReader() {
		return (SpecialDataReader) getReader();
	}

}
