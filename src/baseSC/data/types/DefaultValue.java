package baseSC.data.types;

import java.util.ArrayList;

public class DefaultValue {

	public static final int ID_NULL = 0;
	public static final int ID_INTEGER_0 = 1;
	public static final int ID_INTEGER_MINUS_1 = 2;
	public static final int ID_DOUBLE_0 = 3;
	public static final int ID_LONG_0 = 4;
	public static final int ID_STRING_EMPTY = 5;
	public static final int ID_STRING_NO_REASON = 6;
	public static final int ID_EMPTY_ARRAYLIST = 7;
	public static final int ID_EMPTY_BYTE_ARRAY = 8;
	public static final int ID_BOOLEAN_TRUE = 9;
	public static final int ID_BOOLEAN_FALSE = 10;

	public static final DefaultValue NULL = new DefaultValue(ID_NULL, null);
	public static final DefaultValue INTEGER_0 = new DefaultValue(ID_INTEGER_0, 0);
	public static final DefaultValue DOUBLE_0 = new DefaultValue(ID_DOUBLE_0, 0.0);
	public static final DefaultValue LONG_0 = new DefaultValue(ID_LONG_0, 0l);
	public static final DefaultValue STRING_EMPTY = new DefaultValue(ID_STRING_EMPTY, "");
	public static final DefaultValue STRING_NO_REASON = new DefaultValue(ID_STRING_NO_REASON, "No Reason Given");
	public static final DefaultValue EMPTY_ARRAYLIST = new DefaultValue(ID_EMPTY_ARRAYLIST, new ArrayList<>());
	public static final DefaultValue EMPTY_BYTE_ARRAY = new DefaultValue(ID_EMPTY_BYTE_ARRAY, new byte[0]);
	public static final DefaultValue BOOLEAN_TRUE = new DefaultValue(ID_BOOLEAN_TRUE, true);
	public static final DefaultValue BOOLEAN_FALSE = new DefaultValue(ID_BOOLEAN_FALSE, false);

	private static ArrayList<DefaultValue> DEFAULT_VALUES;

	private Object defaultValue;
	private int id;

	protected DefaultValue(int id, Object defaultValue) {
		this.defaultValue = defaultValue;
		this.id = id;
		if (DEFAULT_VALUES == null) {
			DEFAULT_VALUES = new ArrayList<>();
		}
		DEFAULT_VALUES.add(this);
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public int getId() {
		return id;
	}

	public static DefaultValue fromId(int id) {
		for (int i = 0; i < DEFAULT_VALUES.size(); i++) {
			DefaultValue defaultValue = DEFAULT_VALUES.get(i);
			if (defaultValue.getId() == id) {
				return defaultValue;
			}
		}
		return null;
	}
}
