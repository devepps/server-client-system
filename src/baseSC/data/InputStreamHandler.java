package baseSC.data;

import java.io.DataInputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;

import baseSC.data.dataPackage.DataPackage;
import baseSC.data.dataPackage.DataQueueIn;
import baseSC.data.packageType.PackageManager;
import baseSC.logger.CSBaseLogger;
import data.queue.Queue;

public class InputStreamHandler {

	private class BodyHolder {
		private final long created = System.currentTimeMillis();
		private final Queue<DataPackage> body = new Queue<>();

		private boolean isExpired() {
			return System.currentTimeMillis() - created > DataQueueIn.VALID_DURATION;
		}
	}

	private DataInputStream in;

	private final HashMap<Integer, DataQueueIn> dataStreams = new HashMap<>();
	private final HashMap<Integer, BodyHolder> dataStreamBodys = new HashMap<>();

	public InputStreamHandler(DataInputStream in) {
		this.in = in;
	}

	public DataQueueIn readData() throws Throwable {
		byte[] readData = new byte[PackageManager.PACKAGE_DATA_SIZE];
		in.readFully(readData);
		ByteBuffer buffer = ByteBuffer.wrap(readData);

		CSBaseLogger.debug(Arrays.toString(readData));		
		CSBaseLogger.debug("New-Message: " + buffer.getInt());

		for (int position = 0; position + DataPackage.DATA_INFO_BYTES < PackageManager.PACKAGE_DATA_SIZE;) {
			buffer.position(position);
			int packageId = buffer.getInt();
			int packagePartId = buffer.getInt();
			int contentLength = buffer.getInt();

			int nextPosition = position + DataPackage.DATA_INFO_BYTES + (contentLength >= 0 ? contentLength : 0);

			CSBaseLogger.debug("ContentLength: " + contentLength + " -> " + packagePartId);
			CSBaseLogger.debug("Read-Message: " + packageId + " -> " + packagePartId + " / " + contentLength + " -> " + (PackageManager.PACKAGE_DATA_SIZE - nextPosition));
			if (nextPosition > PackageManager.PACKAGE_DATA_SIZE) {
				break;
			}
			CSBaseLogger.debug("Read-Message: " + packageId + " -> " + 1 );

			byte[] data = Arrays.copyOfRange(buffer.array(), position, nextPosition);
			DataPackage dataPackage = new DataPackage(packageId, packagePartId, contentLength < 0, data,
					Arrays.copyOfRange(data, DataPackage.DATA_INFO_BYTES, data.length));
			CSBaseLogger.debug("Read-Message-To-DataPackage: " + packageId + " -> " +  dataPackage.getContent().length + " ( = " + contentLength + ")");

			position = nextPosition;
			
			CSBaseLogger.debug("Read-Message: " + packageId + " -> " + 2);

			DataQueueIn dataQueue = dataStreams.get(packageId);
			
			CSBaseLogger.debug("Read-Message: " + packageId + " -> " + 3);
			if (packagePartId == -1) {
				CSBaseLogger.debug("No-New-DataPackage: " + packageId + " -> " + packagePartId);
				if (dataQueue == null || dataQueue.isExpired()) {
					dataQueue = new DataQueueIn(dataPackage);
					dataStreams.put(packageId, dataQueue);

					BodyHolder bodyHolder = dataStreamBodys.get(packageId);
					if (bodyHolder != null) {
						if (!bodyHolder.isExpired()) {
							while (!bodyHolder.body.isEmpty()) {
								dataQueue.addDataPackage(bodyHolder.body.pop());
							}
						}
						dataStreamBodys.remove(packageId);
					}
				}

				if (dataQueue.isComplete()) {
					dataStreams.remove(packageId);
					return dataQueue;
				}
			} else {
				if (dataQueue == null || dataQueue.isExpired()) {
					if (dataQueue == null) {
						CSBaseLogger.debug("Null-DataPackage: " + packageId + " -> " + packagePartId);
					} else {
						CSBaseLogger.debug("Expired-DataPackage: " + packageId + " -> (" + packagePartId + " = " + dataQueue.getCount() + ") / " + dataQueue.getBody().length);
					}
					BodyHolder bodyHolder = dataStreamBodys.get(packageId);
					if (bodyHolder == null || bodyHolder.isExpired()) {
						bodyHolder = new BodyHolder();
						dataStreamBodys.put(packageId, bodyHolder);
					}
					bodyHolder.body.add(dataPackage);
				} else {
					dataQueue.addDataPackage(dataPackage);
					CSBaseLogger.debug("Added-DataPackage: " + packageId + " -> (" + packagePartId + " = " + dataQueue.getCount() + ") / " + dataQueue.getBody().length);

					if (dataQueue.isComplete()) {
						dataStreams.remove(packageId);
						return dataQueue;
					}
				}
			}
		}

		return null;
	}

}
