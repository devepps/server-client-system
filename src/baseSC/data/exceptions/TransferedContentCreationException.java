package baseSC.data.exceptions;

import data.exceptions.CustomException;

public class TransferedContentCreationException extends CustomException {

	private static final long serialVersionUID = 1L;
	
	public TransferedContentCreationException(Throwable e) {
		super(e, "An unexpected error occured while trying to create transfered Object.");
	}

	public TransferedContentCreationException() {
		super(null, "An unexpected error occured while trying to create transfered Object.");
	}
}
