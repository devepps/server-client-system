package baseSC.data.dataReader;

public class HeaderData {

	private int packageCount;
	private int packageTypeId;

	public HeaderData(int packageCount, int packageTypeId) {
		super();
		this.packageCount = packageCount;
		this.packageTypeId = packageTypeId;
	}

	public int getPackageCount() {
		return packageCount;
	}

	public int getPackageTypeId() {
		return packageTypeId;
	}

	@Override
	public String toString() {
		return "HeaderData [packageCount=" + packageCount + ", packageTypeId=" + packageTypeId + "]";
	}

}
