package baseSC.data.dataReader;

import baseSC.data.dataPackage.DataPackage;
import baseSC.data.dataPackage.DataQueueOut;
import baseSC.data.packageType.PackageManager;
import baseSC.data.packageType.Pointer;
import baseSC.data.types.SpecialDataReaderType;

public abstract class SpecialDataReader extends DataReader {

	public SpecialDataReader(SpecialDataReaderType dataReaderTyp) {
		super(dataReaderTyp);
	}

	public abstract void handleObjectToBytes(DataQueueOut dataQueue, PackageManager packageManager,
			Object contentObject);

	public abstract int getPackageLength(DataPackage header);

	public abstract Object read(DataPackage[] dataPackages, PackageManager packageManager, Pointer currentPosition);

}
