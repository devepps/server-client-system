package baseSC.data.dataReader;

import baseSC.data.types.DataReaderType;

public abstract class DataReader {

	private DataReaderType dataReaderTyp;

	public DataReader(DataReaderType dataReaderType) {
		this.dataReaderTyp = dataReaderType;
	}

	public DataReaderType getDataReaderTyp() {
		return dataReaderTyp;
	}

	@Override
	public String toString() {
		return "DataReader [dataReaderTyp=" + dataReaderTyp + "]";
	}
}
