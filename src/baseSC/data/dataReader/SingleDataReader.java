package baseSC.data.dataReader;

import baseSC.data.types.SingleDataReaderType;

public abstract class SingleDataReader extends DataReader{

	public SingleDataReader(SingleDataReaderType dataReaderTyp) {
		super(dataReaderTyp);
	}

	public byte[] read(Object readableContent) {
		if (readableContent == null) {
			return new byte[0];
		} else {
			try {
				return readData(readableContent);
			} catch (Throwable e) {
				e.printStackTrace();
				return new byte[0];
			}
		}
	}

	public Object read(byte[] readableData) {
		if (readableData == null) {
			return null;
		} else {
			try {
				return readData(readableData);
			} catch (Throwable e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	protected abstract byte[] readData(Object readableContent) throws Throwable;

	protected abstract Object readData(byte[] readableContent) throws Throwable;

}
