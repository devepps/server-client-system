package baseSC.data.dataReader;

import baseSC.data.dataPackage.DataPackage;
import baseSC.data.packageType.Pointer;
import baseSC.data.types.DataReaderType;
import baseSC.data.types.SingleDataReaderType;

public abstract class MultiDataReader {

	private DataReaderType contentType;
	private SingleDataReaderType headerType;

	public MultiDataReader(DataReaderType contentType, SingleDataReaderType headerType) {
		this.contentType = contentType;
		this.headerType = headerType;
	}

	public DataReaderType getContentType() {
		return contentType;
	}

	public SingleDataReaderType getHeaderType() {
		return headerType;
	}

	public abstract Object getHeader(Object readableContent);

	public abstract Object read(Pointer position, DataPackage[] dataPackages, SingleContentReader singleContentReader);

	public abstract void read(Object readableContent, SingleContentHandler singleContentHandler);

	@Override
	public String toString() {
		return "MultiDataReader [contentType=" + contentType + ", headerType=" + headerType + "]";
	}
}
