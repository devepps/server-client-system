package baseSC.data.dataReader.singleDataReader;

import java.io.UnsupportedEncodingException;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class StringReader_UNICODE extends SingleDataReader {

	public static final String CHARSET = "UTF-32";

	public StringReader_UNICODE() {
		super(SingleDataReaderType.STRING_UNICODE);
	}

	@Override
	public byte[] readData(Object readableContent) throws UnsupportedEncodingException {
		return ((String) readableContent).getBytes(CHARSET);
	}

	@Override
	public Object readData(byte[] readableData) throws UnsupportedEncodingException {
		return new String(readableData, CHARSET);
	}

	@Override
	public String toString() {
		return "StringReader_UNICODE []";
	}
}
