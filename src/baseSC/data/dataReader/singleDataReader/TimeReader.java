package baseSC.data.dataReader.singleDataReader;

import java.time.LocalTime;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class TimeReader extends SingleDataReader {

	public TimeReader() {
		super(SingleDataReaderType.DATE_TIME);
	}

	@Override
	public byte[] readData(Object readableContent) {
		LocalTime dateTime = (LocalTime) readableContent;
		return new byte[] { (byte) dateTime.getHour(), (byte) dateTime.getMinute(), (byte) dateTime.getSecond() };
	}

	@Override
	public Object readData(byte[] readableData) {
		return LocalTime.of(readableData[0], readableData[1], readableData[2]);
	}

	@Override
	public String toString() {
		return "TimeReader []";
	}

}
