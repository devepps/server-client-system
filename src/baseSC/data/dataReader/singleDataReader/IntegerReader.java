package baseSC.data.dataReader.singleDataReader;

import java.nio.ByteBuffer;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;
import baseSC.logger.CSBaseLogger;

public class IntegerReader extends SingleDataReader {

	public IntegerReader() {
		super(SingleDataReaderType.INTEGER);
	}

	@Override
	public byte[] readData(Object readableContent) {
		return ByteBuffer.allocate(Integer.BYTES).putInt((int) readableContent).array();
	}

	@Override
	public Object readData(byte[] readableData) {
		CSBaseLogger.debug("" + readableData.length);
		if (readableData.length == Integer.BYTES) {
			return ByteBuffer.wrap(readableData).getInt();
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "IntegerReader []";
	}

}
