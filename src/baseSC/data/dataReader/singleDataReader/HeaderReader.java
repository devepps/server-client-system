package baseSC.data.dataReader.singleDataReader;

import java.nio.ByteBuffer;

import baseSC.data.dataReader.HeaderData;
import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class HeaderReader extends SingleDataReader {

	public HeaderReader() {
		super(SingleDataReaderType.HEADER);
	}

	@Override
	public byte[] readData(Object readableContent) {
		HeaderData headerData = (HeaderData) readableContent;
		return ByteBuffer.allocate(Integer.BYTES * 2).putInt(headerData.getPackageCount())
				.putInt(headerData.getPackageTypeId()).array();
	}

	@Override
	public Object readData(byte[] readableData) {
		if (readableData.length == Integer.BYTES * 2) {
			ByteBuffer buffer = ByteBuffer.wrap(readableData);
			return new HeaderData(buffer.getInt(), buffer.getInt());
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "HeaderReader []";
	}

}
