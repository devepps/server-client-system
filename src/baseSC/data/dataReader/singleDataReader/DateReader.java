package baseSC.data.dataReader.singleDataReader;

import java.time.LocalDate;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class DateReader extends SingleDataReader {

	public DateReader() {
		super(SingleDataReaderType.DATE);
	}

	@Override
	public byte[] readData(Object readableContent) {
		LocalDate dateTime = (LocalDate) readableContent;
		return new byte[] { (byte) dateTime.getDayOfMonth(), (byte) dateTime.getMonthValue(),
				(byte) (dateTime.getYear() - 2000) };
	}

	@Override
	public Object readData(byte[] readableData) {
		return LocalDate.of(readableData[2] + 2000, readableData[1], readableData[0]);
	}

	@Override
	public String toString() {
		return "DateReader []";
	}

}
