package baseSC.data.dataReader.singleDataReader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;
import baseSC.logger.CSBaseLogger;

public class SerializableReader extends SingleDataReader {

	public SerializableReader() {
		super(SingleDataReaderType.SERIALIZABLE);
	}

	@Override
	protected byte[] readData(Object readableContent) throws Throwable {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		ObjectOutputStream outputStream = new ObjectOutputStream(bytes);
		outputStream.writeObject(readableContent);
		outputStream.flush();
		byte[] data = bytes.toByteArray();
		CSBaseLogger.debug(toString() + " reads \"" + readableContent + "\" to " + data);
		outputStream.close();
		bytes.close();
		return data;
	}

	@Override
	protected Object readData(byte[] readableContent) throws Throwable {
		ByteArrayInputStream bytes = new ByteArrayInputStream(readableContent);
		ObjectInputStream inputStream = new ObjectInputStream(bytes);
		Object data = inputStream.readObject();
		CSBaseLogger.debug(toString() + " reads \"" + readableContent + "\" to " + data);
		inputStream.close();
		bytes.close();
		return data;
	}

	@Override
	public String toString() {
		return "SerializableReader []";
	}
}
