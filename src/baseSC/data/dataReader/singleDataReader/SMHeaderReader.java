package baseSC.data.dataReader.singleDataReader;

import java.nio.ByteBuffer;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.subscriber.SMHeader;
import baseSC.data.types.SingleDataReaderType;

public class SMHeaderReader extends SingleDataReader {

	private static final int BYTE_COUNT = (Long.BYTES * 3) + Integer.BYTES;

	public SMHeaderReader() {
		super(SingleDataReaderType.SUBSCRIBED_MESSAGES_HEADER);
	}

	@Override
	protected byte[] readData(Object readableContent) throws Throwable {
		SMHeader headerData = (SMHeader) readableContent;
		return ByteBuffer.allocate(BYTE_COUNT).putLong(headerData.getId()).putLong(headerData.getValidDuration())
				.putLong(headerData.getCreated()).putInt(headerData.getStatusId()).array();
	}

	@Override
	protected Object readData(byte[] readableData) throws Throwable {
		if (readableData.length == BYTE_COUNT) {
			ByteBuffer buffer = ByteBuffer.wrap(readableData);
			return new SMHeader(buffer.getLong(), buffer.getLong(), buffer.getLong(), buffer.getInt());
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "SMHeaderReader []";
	}
}
