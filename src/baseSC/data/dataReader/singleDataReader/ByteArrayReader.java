package baseSC.data.dataReader.singleDataReader;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class ByteArrayReader extends SingleDataReader {

	public ByteArrayReader() {
		super(SingleDataReaderType.BYTE_ARRAY);
	}

	@Override
	public byte[] readData(Object readableContent) {
		return (byte[]) readableContent;
	}

	@Override
	public Object readData(byte[] readableData) {
		return readableData;
	}

	@Override
	public String toString() {
		return "ByteArrayReader []";
	}
}
