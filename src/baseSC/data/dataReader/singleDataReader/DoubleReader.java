package baseSC.data.dataReader.singleDataReader;

import java.nio.ByteBuffer;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class DoubleReader extends SingleDataReader {

	public DoubleReader() {
		super(SingleDataReaderType.DOUBLE);
	}

	@Override
	public byte[] readData(Object readableContent) {
		return ByteBuffer.allocate(Double.BYTES).putDouble((double) readableContent).array();
	}

	@Override
	public Object readData(byte[] readableData) {
		if (readableData.length == Double.BYTES) {
			return ByteBuffer.wrap(readableData).getDouble();
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "DoubleReader []";
	}

}
