package baseSC.data.dataReader.singleDataReader;

import java.time.LocalDateTime;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class DateTimeReader extends SingleDataReader {

	public DateTimeReader() {
		super(SingleDataReaderType.DATE_TIME);
	}

	@Override
	public byte[] readData(Object readableContent) {
		LocalDateTime dateTime = (LocalDateTime) readableContent;
		byte[] data = new byte[] { 
				(byte) dateTime.getDayOfMonth(),
				(byte) dateTime.getMonthValue(),
				(byte) (dateTime.getYear() - 2000),
				(byte) dateTime.getHour(),
				(byte) dateTime.getMinute(),
				(byte) dateTime.getSecond()};
		return data;
	}

	@Override
	public Object readData(byte[] readableData) {
		return LocalDateTime.of(readableData[2] + 2000, readableData[1], readableData[0], readableData[3], readableData[4], readableData[5]);
	}

	@Override
	public String toString() {
		return "DateTimeReader []";
	}

}
