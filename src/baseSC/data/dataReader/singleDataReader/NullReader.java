package baseSC.data.dataReader.singleDataReader;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class NullReader extends SingleDataReader {

	public NullReader() {
		super(SingleDataReaderType.NULL);
	}

	@Override
	public byte[] readData(Object readableContent) {
		return new byte[0];
	}

	@Override
	public Object readData(byte[] readableData) {
		return null;
	}

	@Override
	public String toString() {
		return "NullReader []";
	}
}
