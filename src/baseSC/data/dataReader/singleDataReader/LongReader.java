package baseSC.data.dataReader.singleDataReader;

import java.nio.ByteBuffer;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class LongReader extends SingleDataReader {

	public LongReader() {
		super(SingleDataReaderType.LONG);
	}

	@Override
	public byte[] readData(Object readableContent) {
		return ByteBuffer.allocate(Long.BYTES).putLong((long) readableContent).array();
	}

	@Override
	public Object readData(byte[] readableData) {
		if (readableData.length == Long.BYTES) {
			return ByteBuffer.wrap(readableData).getLong();
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "LongReader []";
	}

}
