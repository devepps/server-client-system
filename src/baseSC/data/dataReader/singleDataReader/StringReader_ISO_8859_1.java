package baseSC.data.dataReader.singleDataReader;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class StringReader_ISO_8859_1 extends SingleDataReader {

	private static final String CHARSET_NAME = "ISO_8859_1";
	private static final Charset CHARSET = lookupCharset(CHARSET_NAME);

    private static Charset lookupCharset(String csn) {
        if (Charset.isSupported(csn)) {
            try {
                return Charset.forName(csn);
            } catch (UnsupportedCharsetException x) {
                throw new Error(x);
            }
        }
        return null;
    }

	public StringReader_ISO_8859_1() {
		super(SingleDataReaderType.STRING_ISO_8859_1);
	}

	@Override
	public byte[] readData(Object readableContent) throws UnsupportedEncodingException {
		return ((String) readableContent).getBytes(CHARSET);
	}

	@Override
	public Object readData(byte[] readableData) throws UnsupportedEncodingException {
		return new String(readableData, CHARSET);
	}

	@Override
	public String toString() {
		return "StringReader_ISO_8859_1 []";
	}
}
