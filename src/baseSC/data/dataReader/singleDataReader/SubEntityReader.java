package baseSC.data.dataReader.singleDataReader;

import java.util.Arrays;

import baseSC.data.dataPackage.DataPackage;
import baseSC.data.dataPackage.DataQueueOut;
import baseSC.data.dataReader.HeaderData;
import baseSC.data.dataReader.SpecialDataReader;
import baseSC.data.packageType.PackageManager;
import baseSC.data.packageType.PackageType;
import baseSC.data.packageType.Pointer;
import baseSC.data.types.SingleDataReaderType;
import baseSC.data.types.SpecialDataReaderType;
import baseSC.logger.CSBaseLogger;

public class SubEntityReader extends SpecialDataReader {

	public SubEntityReader() {
		super(SpecialDataReaderType.SUBENTITY);
	}

	@Override
	public void handleObjectToBytes(DataQueueOut dataQueue, PackageManager packageManager, Object contentObject) {
		PackageType<?> subType = contentObject == null ? null : packageManager.getPackageType(contentObject.getClass());
		if (subType != null) {
			DataQueueOut subOut = subType.readPackagable(packageManager, contentObject, dataQueue.getPackageId());
			dataQueue.registerSubDataQueue(subOut);
			CSBaseLogger.debug("send subEntity " + subType.getPackageTypeId() + " : [" + dataQueue.getBody().getLength() + "] -> (" + contentObject + " & " + subType + ")");
		} else {
			dataQueue.registerContentPackage(null, SingleDataReaderType.NULL);
			CSBaseLogger.debug("send subEntity " + null + " : [" + dataQueue.getBody().getLength() + "] -> (" + contentObject + " & " + subType + ")");
		}
	}

	@Override
	public int getPackageLength(DataPackage header) {
		HeaderData subHeader = (HeaderData) SingleDataReaderType.HEADER.getSingleDataReader().read(header.getContent());
		return subHeader.getPackageCount() + 1;
	}

	@Override
	public Object read(DataPackage[] dataPackages, PackageManager packageManager,
			Pointer currentPosition) {
		HeaderData subHeader = (HeaderData) SingleDataReaderType.HEADER.getSingleDataReader()
				.read(dataPackages[currentPosition.getPosition()].getContent());

		PackageType<?> subPackageType = subHeader == null ? null
				: packageManager.getPackageType(subHeader.getPackageTypeId());

		CSBaseLogger.debug("found subEntity: " + subHeader + " & " + subPackageType + ")");

		Object content = null;
		if (subPackageType != null) {
			content = subPackageType.readData(packageManager,
					Arrays.copyOfRange(dataPackages, currentPosition.getPosition() + 1, currentPosition.getPosition() + 1 + subHeader.getPackageCount()));
		}
		currentPosition.move(subHeader.getPackageCount() + 1);
		return content;
	}

	@Override
	public String toString() {
		return "SubEntityReader []";
	}
}
