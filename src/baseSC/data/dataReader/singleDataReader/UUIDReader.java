package baseSC.data.dataReader.singleDataReader;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class UUIDReader extends SingleDataReader {

	public UUIDReader() {
		super(SingleDataReaderType.DATE_TIME);
	}

	@Override
	public byte[] readData(Object readableContent) {
		UUID uuid = (UUID) readableContent;
		return uuid.toString().getBytes(StandardCharsets.UTF_8);
	}

	@Override
	public Object readData(byte[] readableData) {
		if(readableData.length == 0) {
			return null;
		}
		return UUID.fromString(new String(readableData, StandardCharsets.UTF_8));
	}

	@Override
	public String toString() {
		return "UUIDReader []";
	}

}
