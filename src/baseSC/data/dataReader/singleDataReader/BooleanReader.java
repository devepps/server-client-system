package baseSC.data.dataReader.singleDataReader;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class BooleanReader extends SingleDataReader {

	public BooleanReader() {
		super(SingleDataReaderType.BOOLEAN);
	}

	@Override
	public byte[] readData(Object readableContent) {
		return new byte[] { (byte) (((Boolean) readableContent) ? 1 : 0) };
	}

	@Override
	public Object readData(byte[] readableData) {
		if (readableData.length == 1) {
			return readableData[0] == 1;
		}
		return null;
	}

	@Override
	public String toString() {
		return "BooleanReader []";
	}

}
