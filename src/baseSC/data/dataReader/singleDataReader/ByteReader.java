package baseSC.data.dataReader.singleDataReader;

import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.types.SingleDataReaderType;

public class ByteReader extends SingleDataReader {

	public ByteReader() {
		super(SingleDataReaderType.BYTE);
	}

	@Override
	public byte[] readData(Object readableContent) {
		return new byte[] { (byte) readableContent };
	}

	@Override
	public Object readData(byte[] readableData) {
		if (readableData.length == 1) {
			return readableData[0];
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "ByteReader []";
	}
}
