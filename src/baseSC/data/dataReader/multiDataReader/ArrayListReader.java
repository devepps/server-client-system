package baseSC.data.dataReader.multiDataReader;

import java.util.ArrayList;
import java.util.Arrays;

import baseSC.data.dataPackage.DataPackage;
import baseSC.data.dataReader.MultiDataReader;
import baseSC.data.dataReader.SingleContentHandler;
import baseSC.data.dataReader.SingleContentReader;
import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.dataReader.SpecialDataReader;
import baseSC.data.packageType.Pointer;
import baseSC.data.types.DataReaderType;
import baseSC.data.types.SingleDataReaderType;
import baseSC.data.types.SpecialDataReaderType;
import baseSC.logger.CSBaseLogger;

public class ArrayListReader extends MultiDataReader {

	public ArrayListReader(DataReaderType contentType) {
		super(contentType, SingleDataReaderType.INTEGER);
	}

	@Override
	public Object getHeader(Object readableContent) {
		if (readableContent == null || !(readableContent instanceof ArrayList)) {
			return -1;
		}
		return ((ArrayList<?>) readableContent).size();
	}

	@Override
	public void read(Object readableContent, SingleContentHandler singleContentHandler) {
		if (readableContent == null || !(readableContent instanceof ArrayList)) {
			singleContentHandler.handle(null);
			return;
		}
		ArrayList<?> arrayList = (ArrayList<?>) readableContent;
		for (Object obj : arrayList) {
			singleContentHandler.handle(obj);	
		}
	}

	@Override
	public Object read(Pointer position, DataPackage[] dataPackages, SingleContentReader singleContentReader) {		
		int startPos = position.getPosition();
		SingleDataReader singleDataReader = getHeaderType().getSingleDataReader();
		byte[] data = dataPackages[position.getPosition()].getContent();
		CSBaseLogger.debug("Read1: " + position + ", " + Arrays.toString(dataPackages) + ", " + singleContentReader + " -> " + dataPackages[position.getPosition()]);
		CSBaseLogger.debug("Read2: " + singleDataReader + " -> " + data);

		Object read = singleDataReader.read(data);
		CSBaseLogger.debug("Read3: " + read);		
		int size = (int) read;
		position.move(1);

		ArrayList<Object> arrayList = new ArrayList<>();
		if (size >= 1) {
			int maxLength = 0;
			if (getContentType() instanceof SpecialDataReaderType) {
				SpecialDataReader sdr = ((SpecialDataReaderType) getContentType()).getSpecialDataReader();
				for (int i = 0; i < size; i++) {
					maxLength += sdr.getPackageLength(dataPackages[position.getPosition() + maxLength]);
				}
			} else {
				maxLength = size + 1;
			}
			while (maxLength + startPos > position.getPosition()) {
				arrayList.add(singleContentReader.read());
			}
		}

		return arrayList;

	}

}
