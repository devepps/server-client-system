package baseSC.data.dataReader.multiDataReader;

import baseSC.data.dataPackage.DataPackage;
import baseSC.data.dataReader.MultiDataReader;
import baseSC.data.dataReader.SingleContentHandler;
import baseSC.data.dataReader.SingleContentReader;
import baseSC.data.packageType.Pointer;
import baseSC.data.types.DataReaderType;
import baseSC.data.types.SingleDataReaderType;
import baseSC.logger.CSBaseLogger;

public class MultiSingleDataReader extends MultiDataReader {

	public MultiSingleDataReader(DataReaderType contentType) {
		super(contentType, SingleDataReaderType.NULL);
	}

	@Override
	public Object getHeader(Object readableContent) {
		return null;
	}

	@Override
	public void read(Object readableContent, SingleContentHandler singleContentHandler) {
		CSBaseLogger.debug("handle MultiSingleContent: " + readableContent + singleContentHandler);
		singleContentHandler.handle(readableContent);
	}

	@Override
	public Object read(Pointer position, DataPackage[] dataPackages, SingleContentReader singleContentReader) {
		return singleContentReader.read();
	}

}
