package baseSC.data.internelDTO;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.internel.HeartBeat")
public class HeartBeatDTO implements DTO{

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_LONG, defaultValueId = DefaultValue.ID_LONG_0)
	private long currentTime;

	public HeartBeatDTO() {
		this.currentTime = System.currentTimeMillis();
	}

	public long getCurrentTime() {
		return currentTime;
	}

	@Override
	public DTO copy() {
		HeartBeatDTO heartBeatDTO = new HeartBeatDTO();
		heartBeatDTO.currentTime = this.currentTime;
		return heartBeatDTO;
	}
}
