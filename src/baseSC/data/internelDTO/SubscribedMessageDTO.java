package baseSC.data.internelDTO;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.subscriber.SMHeader;
import baseSC.data.types.SingleDataReaderType;
import baseSC.data.types.SpecialDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.internel.SubscribedMessage")
public class SubscribedMessageDTO implements DTO {

	public SubscribedMessageDTO() {
	}

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_SUBSCRIBED_MESSAGES_HEADER)
	private SMHeader smHeader;

	@PackagableContent(contentTypeKey = SpecialDataReaderType.KEY_SUBENTITY)
	private DTO content;

	public SubscribedMessageDTO(SMHeader smHeader, DTO content) {
		super();
		this.smHeader = smHeader;
		this.content = content;
	}

	public SMHeader getSmHeader() {
		return smHeader;
	}

	public DTO getContent() {
		return content;
	}

	@Override
	public DTO copy() {
		return new SubscribedMessageDTO(smHeader, (DTO) content.copy());
	}

	@Override
	public String toString() {
		return "SubscribedMessageDTO [smHeader=" + smHeader + ", content=" + content + "]";
	}
}
