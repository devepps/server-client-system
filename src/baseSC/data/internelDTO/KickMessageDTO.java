package baseSC.data.internelDTO;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.internel.Kick")
public class KickMessageDTO implements DTO{
	
	public KickMessageDTO(){};

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_STRING_NO_REASON)
	private String reason;

	public KickMessageDTO(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	@Override
	public DTO copy() {
		return new KickMessageDTO(reason);
	}
}
