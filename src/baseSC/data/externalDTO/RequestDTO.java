package baseSC.data.externalDTO;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.external.Request")
public class RequestDTO implements DTO {

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String request;

	public RequestDTO() {
		super();
	}

	public RequestDTO(String request) {
		super();
		this.request = request;
	}

	public String getRequest() {
		int index = request.indexOf('?');
		return request.substring(0, index == -1 ? request.length() : index);
	}

	public String getContent(int index) {
		String[] content = request.split("?");
		return content.length - 1 > index ? content[index - 1] : "";
	}

	@Override
	public DTO copy() {
		return new RequestDTO(request);
	}

	@Override
	public String toString() {
		return "RequestDTO [request=" + request + "]";
	}
}
