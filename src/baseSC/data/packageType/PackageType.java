package baseSC.data.packageType;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import baseSC.data.dataPackage.DataPackage;
import baseSC.data.dataPackage.DataQueueOut;
import baseSC.data.exceptions.TransferedContentCreationException;
import baseSC.data.internelDTO.HeartBeatDTO;
import baseSC.data.types.DataReaderType;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.MultiDataReaderType;
import baseSC.data.types.SingleDataReaderType;
import baseSC.data.types.SpecialDataReaderType;
import baseSC.logger.CSBaseLogger;
import config.ConfigEnvironment;
import data.queue.Queue;

public class PackageType<ContentType> {

	private PackagableField[] packagableFields;
	private Class<ContentType> packagableClass;

	private int packageTypeId;

	public int getPackageTypeId() {
		return packageTypeId;
	}

	public PackageType(Class<ContentType> packagableClass) {
		PackagableEntity entityInfos = packagableClass.getDeclaredAnnotation(PackagableEntity.class);
		this.packageTypeId = Integer
				.parseInt(ConfigEnvironment.getProperties(entityInfos.configKey()).get(entityInfos.configKeyOrder()));

		CSBaseLogger.debug("packageType: \"" + packagableClass.getName() + "\" has been registered via Key: \""
				+ entityInfos.configKey() + "\" : " + entityInfos.configKeyOrder() + " as " + packageTypeId);

		this.packagableClass = packagableClass;
		try {
			this.packagableClass.getConstructor().setAccessible(true);
		} catch (SecurityException | NoSuchMethodException e) {
			e.printStackTrace();
		}
		Queue<PackagableField> packagableFields = new Queue<>();
		for (Field field : packagableClass.getDeclaredFields()) {
			PackagableContent packageInfos = field.getDeclaredAnnotation(PackagableContent.class);
			if (packageInfos != null) {
				field.setAccessible(true);
				packagableFields.add(new PackagableField(field,
						MultiDataReaderType.getType(packageInfos.dataTypeKey())
								.create(DataReaderType.getType(packageInfos.contentTypeKey())),
						DefaultValue.fromId(packageInfos.defaultValueId()), packagableFields.getLength()));
			}
		}

		this.packagableFields = new PackagableField[packagableFields.getLength()];
		while (!packagableFields.isEmpty()) {
			PackagableField field = packagableFields.pop();
			this.packagableFields[field.getFieldId()] = field;
		}
	}

	public DataQueueOut readPackagable(PackageManager packageManager, Object packagableObject, int packageId) {
		DataQueueOut dataQueue = new DataQueueOut(packageId, packageTypeId);
		for (int i = 0; i < packagableFields.length; i++) {
			PackagableField field = packagableFields[i];
			Object obj = null;
			try {
				obj = field.getField().get(packagableObject);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}

			Object multiHeader = field.getDataReader().getHeader(obj);
			if (multiHeader != null) {
				dataQueue.registerContentPackage(multiHeader, field.getDataReader().getHeaderType());
			}
			CSBaseLogger.debug("MultiReadToBytes: " + obj + " -> " + field.getDataReader());
			field.getDataReader().read(obj, (Object contentObject) -> {
				CSBaseLogger.debug("SingleReadToBytes: " + contentObject);
				if (field.getDataReader().getContentType() instanceof SingleDataReaderType) {
					dataQueue.registerContentPackage(contentObject, (SingleDataReaderType) field.getDataReader().getContentType());					
				} else {
					((SpecialDataReaderType) field.getDataReader().getContentType()).getSpecialDataReader().handleObjectToBytes(dataQueue, packageManager, contentObject);
				}
			});
		}
		dataQueue.createHeader();
		return dataQueue;
	}

	public ContentType readData(PackageManager packageManager, DataPackage[] dataPackages) {
		try {
			ContentType content = this.packagableClass.getConstructor().newInstance();
			Pointer position = new Pointer();
			for (int i = 0; i < packagableFields.length; i++) {
				PackagableField field = packagableFields[i];

				CSBaseLogger.debug("ReadMulti: " + field.getDataReader() + " -> " + position);
				Object object = field.getDataReader()
						.read(position, dataPackages, () -> {
							if (!packagableClass.equals(HeartBeatDTO.class)) {
								CSBaseLogger.debug("ReadSingle: " + field.getDataReader());
								CSBaseLogger.debug("ReadSingle: " + field.getDataReader().getContentType());
							}
							if (field.getDataReader().getContentType() instanceof SingleDataReaderType) {
								Object subContent = ((SingleDataReaderType) field.getDataReader().getContentType())
										.getSingleDataReader()
										.read(dataPackages[position.getPosition()].getContent());
								position.move(1);
								return subContent;
							} else {
								if (!packagableClass.equals(HeartBeatDTO.class)) {
									CSBaseLogger.debug("ReadSingle: " + ((SpecialDataReaderType) field.getDataReader().getContentType()).getSpecialDataReader());
								}
								Object subContent = ((SpecialDataReaderType) field.getDataReader().getContentType())
										.getSpecialDataReader()
										.read(dataPackages, packageManager, position);
								return subContent;
							}
						});
				CSBaseLogger.debug("Read-Multi: " + field.getDataReader() + " -> " + position + "|" + i + " -> " + object);
				if (object == null) {
					object = field.getDefaultValue().getDefaultValue();
				}
				field.getField().set(content, object);
			}
			return content;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			throw new TransferedContentCreationException(e);
		}
	}

	@Override
	public String toString() {
		return "PackageType [packagableFields=" + Arrays.toString(packagableFields) + ", packagableClass="
				+ packagableClass + ", packageTypeId=" + packageTypeId + "]";
	}
}
