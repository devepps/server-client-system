package baseSC.data.packageType;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import baseSC.data.types.DefaultValue;
import baseSC.data.types.MultiDataReaderType;

@Retention(RUNTIME)
@Target(FIELD)
public @interface PackagableContent {

	public String dataTypeKey() default MultiDataReaderType.KEY_SINGLE;
	public String contentTypeKey();
	public int defaultValueId() default DefaultValue.ID_NULL;

}
