package baseSC.data.packageType;

public class PackageTypeKey {

	private Class<?> entityClass;
	private int id;
	
	public PackageTypeKey(Class<?> entityClass, int id) {
		super();
		this.entityClass = entityClass;
		this.id = id;
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PackageTypeKey) {
			PackageTypeKey key = (PackageTypeKey) obj;
			if (key.id == id) {
				return true;
			} else if (entityClass.getName().equals(key.entityClass.getName())
					&& entityClass.getPackage().equals(key.getClass().getPackage())) {
				return true;
			}
		} else if (obj instanceof Integer) {
			return (id == (int) obj);
		} else if (obj instanceof Class<?>) {
			Class<?> key = (Class<?>) obj;
			if (entityClass.getName().equals(key.getName())
					&& entityClass.getPackage().equals(key.getPackage())) {
				return true;
			}
		}
		return false;
	}
	
	
	
}
