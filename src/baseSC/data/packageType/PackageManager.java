package baseSC.data.packageType;

import java.nio.ByteBuffer;

import baseSC.data.DTO;
import baseSC.data.dataPackage.DataPackage;
import baseSC.data.dataPackage.DataQueueIn;
import baseSC.data.dataPackage.DataQueueOut;
import baseSC.data.dataReader.HeaderData;
import baseSC.data.dataReader.SingleDataReader;
import baseSC.data.internelDTO.HeartBeatDTO;
import baseSC.data.internelDTO.KickMessageDTO;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.types.DataReaderType;
import baseSC.data.types.SingleDataReaderType;
import baseSC.logger.CSBaseLogger;
import collection.sync.SyncHashMap;
import data.queue.Queue;

public class PackageManager {

	public static final int PACKAGE_DATA_SIZE = 1024;

	private final SyncHashMap<Class<?>, PackageType<?>> packageTypesClassKey = new SyncHashMap<>();
	private final SyncHashMap<Integer, PackageType<?>> packageTypesIdKey = new SyncHashMap<>();

	public PackageManager () {
		DataReaderType.load();
		this.createPackageType(HeartBeatDTO.class);
		this.createPackageType(KickMessageDTO.class);
		this.createPackageType(SubscribedMessageDTO.class);
	}

	public <ContentType> PackageType<?> createPackageType(Class<ContentType> packagableObject) {
		if (packageTypesClassKey.containsKey(packagableObject)) {
			CSBaseLogger.debug("Error occured: duplicated PackageType: " + packagableObject);
			return packageTypesClassKey.get(packagableObject);
		}
		PackageType<ContentType> packageType = new PackageType<ContentType>(packagableObject);
		packageTypesClassKey.put(packagableObject, packageType);
		packageTypesIdKey.put(packageType.getPackageTypeId(), packageType);
		return packageType;
	}

	public Queue<byte[]> readDTO(Object object, int packageId) {
		PackageType<?> packageType = packageTypesClassKey.get(object.getClass());
		if(packageType == null) {
			if(object != null && object instanceof DTO) {
				packageType = createPackageType(object.getClass());
			} else {
				CSBaseLogger.debug("Error occured: unknown package: " + object);
			}
		}

		DataQueueOut dataQueue = packageType.readPackagable(this, object, packageId);

		Queue<byte[]> sendData = new Queue<>();
		ByteBuffer buffer = ByteBuffer.allocate(PACKAGE_DATA_SIZE);
		buffer.put(dataQueue.getHeader().getPackageData());

		while (!dataQueue.getBody().isEmpty()) {
			DataPackage dataPackage = dataQueue.getBody().get();
			dataQueue.getBody().remove();

			CSBaseLogger.debug("send Package " + dataPackage.getPackagePart() + " -> " + dataPackage.getContent().length);

			if (buffer.position() + dataPackage.getPackageData().length > PACKAGE_DATA_SIZE) {
				sendData.add(buffer.array());
				buffer = ByteBuffer.allocate(PACKAGE_DATA_SIZE);
			}
			buffer.put(dataPackage.getPackageData());
		}
		sendData.add(buffer.array());

		return sendData;
	}

	public Object readDataQueue(DataQueueIn dataQueue) {
		HeaderData headerData = (HeaderData) ((SingleDataReader) SingleDataReaderType.HEADER.getReader()).read(dataQueue.getHeader().getContent());
		PackageType<?> packageType = packageTypesIdKey.get(headerData.getPackageTypeId());

		return packageType.readData(this, dataQueue.getBody());
	}

	public PackageType<?> getPackageType(Class<? extends Object> dtoClass) {
		return packageTypesClassKey.getSyncManager().syncronize(() -> {
			CSBaseLogger.debug("Found package for Class " + dtoClass + " : " + packageTypesClassKey.get(dtoClass));
			for(Class<?> key : packageTypesClassKey.keySet()) {
				CSBaseLogger.debug("It exist package for Class " + key + " : " + packageTypesClassKey.get(key));				
			}
			return packageTypesClassKey.get(dtoClass);
		});
	}

	public PackageType<?> getPackageType(int packageTypeId) {
		return packageTypesIdKey.getSyncManager().syncronize(() -> {
			return packageTypesIdKey.get(packageTypeId);
		});
	}

}
