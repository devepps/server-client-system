package baseSC.data.packageType;

import java.lang.reflect.Field;

import baseSC.data.dataReader.MultiDataReader;
import baseSC.data.types.DefaultValue;

public class PackagableField {

	private Field field;
	private MultiDataReader dataReader;
	private DefaultValue defaultValue;
	private int fieldId;
	
	public PackagableField(Field field, MultiDataReader dataReader, DefaultValue defaultValue, int fieldId) {
		super();
		this.field = field;
		this.dataReader = dataReader;
		this.defaultValue = defaultValue;
		this.fieldId = fieldId;
	}

	public Field getField() {
		return field;
	}

	public MultiDataReader getDataReader() {
		return dataReader;
	}

	public DefaultValue getDefaultValue() {
		return defaultValue;
	}

	public int getFieldId() {
		return fieldId;
	}

	@Override
	public String toString() {
		return "PackagableField [field=" + field + ", dataReader=" + dataReader + ", defaultValue=" + defaultValue
				+ ", fieldId=" + fieldId + "]";
	}
}
