package baseSC.data.packageType;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface PackagableEntity {
	
	public String configKey();
	public int configKeyOrder() default 0;

}
