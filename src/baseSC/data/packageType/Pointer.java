package baseSC.data.packageType;

public class Pointer {

	private int position = 0;

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void move(int amount) {
		this.position += amount;
	}

	@Override
	public String toString() {
		return "Pointer [position=" + position + "]";
	}
}
