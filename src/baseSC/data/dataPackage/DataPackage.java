package baseSC.data.dataPackage;

import java.nio.ByteBuffer;
import java.util.Arrays;

import baseSC.data.types.SingleDataReaderType;
import baseSC.logger.CSBaseLogger;

public class DataPackage {

	public static final int DATA_INFO_BYTES = Integer.BYTES * 3;

	private int packageId;
	private int packagePart;
	private boolean isNull;
	private byte[] data;
	private byte[] content;

	public DataPackage(int packageId, int packagePart, boolean isNull, byte[] data, byte[] content) {
		super();
		this.packageId = packageId;
		this.packagePart = packagePart;
		this.isNull = isNull;
		this.data = data;
		this.content = content;
	}

	public DataPackage(int packageId, int packagePart, boolean isNull, byte[] content) {
		super();
		this.packageId = packageId;
		this.packagePart = packagePart;
		this.isNull = isNull;
		this.content = content;
		ByteBuffer buffer = ByteBuffer.allocate(DATA_INFO_BYTES + content.length);
		buffer.putInt(packageId);
		buffer.putInt(packagePart);
		buffer.putInt(isNull ? -1 : content.length);
		buffer.put(content);
		this.data = buffer.array();
	}

	public DataPackage(int packageId, int packagePart, SingleDataReaderType readerType, Object readableObject) {
		super();
		this.packageId = packageId;
		this.packagePart = packagePart;
		this.isNull = readableObject == null;
		this.content = isNull ? new byte[0] : readerType.getSingleDataReader().read(readableObject);
		ByteBuffer buffer = ByteBuffer.allocate(DATA_INFO_BYTES + content.length);
		buffer.putInt(packageId);
		buffer.putInt(packagePart);
		buffer.putInt(isNull ? -1 : content.length);
		buffer.put(content);
		this.data = buffer.array();
		CSBaseLogger.debug("DataPackageCreation-info: " + packageId + ", " + packagePart + ", " + readerType + ", " + readableObject);
		CSBaseLogger.debug("DataPackageCreated-info: " + this);
	}

	public DataPackage(int packageId, int packagePart) {
		super();
		this.packageId = packageId;
		this.packagePart = packagePart;
		this.isNull = false;
		this.content = new byte[0];
		ByteBuffer buffer = ByteBuffer.allocate(DATA_INFO_BYTES);
		buffer.putInt(packageId);
		buffer.putInt(packagePart);
		buffer.putInt(-1);
		this.data = buffer.array();
	}

	public int getPackageId() {
		return packageId;
	}

	public int getPackagePart() {
		return packagePart;
	}

	public byte[] getPackageData() {
		return data;
	}

	public boolean isNull() {
		return isNull;
	}

	public byte[] getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "DataPackage [packageId=" + packageId + ", packagePart=" + packagePart + ", isNull=" + isNull + ", data="
				+ Arrays.toString(data) + ", content=" + Arrays.toString(content) + "]";
	}
}
