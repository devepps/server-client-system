package baseSC.data.dataPackage;

import java.util.Arrays;

import baseSC.data.dataReader.HeaderData;
import baseSC.data.types.SingleDataReaderType;

public class DataQueueIn {

	public static final long VALID_DURATION = 60 * 1000;

	private DataPackage header;
	private DataPackage[] body;  

	private int packageId;
	private int packageTypeId;
	private int count = 0;
	private long created = System.currentTimeMillis();

	public DataQueueIn (DataPackage headerPackage) {
		HeaderData headerData = (HeaderData) SingleDataReaderType.HEADER.getSingleDataReader().read(headerPackage.getContent());
		this.body = new DataPackage[headerData.getPackageCount()];
		this.packageTypeId = headerData.getPackageTypeId();
		this.packageId = headerPackage.getPackageId();
		this.header = headerPackage;
	}

	public void addDataPackage(DataPackage dataPackage) {
		this.body[dataPackage.getPackagePart()] = dataPackage;
		count++;
	}

	public DataPackage getHeader() {
		return header;
	}

	public boolean isComplete() {
		return count == body.length;
	}

	public DataPackage[] getBody() {
		return body;
	}

	public boolean isExpired() {
		return System.currentTimeMillis() - created > VALID_DURATION;
	}

	public int getPackageId() {
		return packageId;
	}

	public int getPackageTypeId() {
		return packageTypeId;
	}

	public int getCount() {
		return count;
	}

	@Override
	public String toString() {
		return "DataQueueIn [header=" + header + ", body=" + Arrays.toString(body) + ", packageId=" + packageId
				+ ", packageTypeId=" + packageTypeId + ", count=" + count + ", created=" + created + "]";
	}
}
