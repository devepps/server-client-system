package baseSC.data.dataPackage;

import baseSC.data.dataReader.HeaderData;
import baseSC.data.types.SingleDataReaderType;
import data.queue.Queue;

public class DataQueueOut {

	private DataPackage header;
	private final Queue<DataPackage> body = new Queue<>();

	private int packageId;
	private int packageTypeId;

	public DataQueueOut(int packageId, int packageTypeId) {
		this.packageId = packageId;
		this.packageTypeId = packageTypeId;
	}

	public void registerContentPackage(Object readableContent, SingleDataReaderType dataReaderType) {
		DataPackage dataPackage = new DataPackage(this.packageId, body.getLength(), dataReaderType, readableContent);
		this.body.add(dataPackage);
	}

	public void createHeader() {
		HeaderData headerData = new HeaderData(body.getLength(), packageTypeId);
		this.header = new DataPackage(packageId, -1, SingleDataReaderType.HEADER, headerData);
	}

	public DataPackage getHeader() {
		return header;
	}

	public Queue<DataPackage> getBody() {
		return body;
	}

	public int getPackageId() {
		return packageId;
	}

	@Override
	public String toString() {
		return "DataQueueOut [header=" + header + ", body=" + body + ", packageId=" + packageId + ", packageTypeId="
				+ packageTypeId + "]";
	}

	public void registerSubDataQueue(DataQueueOut subOut) {
		this.body.add(new DataPackage(packageId, body.getLength(), SingleDataReaderType.HEADER,
				new HeaderData(subOut.body.getLength(), subOut.packageTypeId)));
		while (!subOut.body.isEmpty()) {
			DataPackage dataPackage = subOut.body.pop();
			this.body.add(new DataPackage(dataPackage.getPackageId(), body.getLength(),
					dataPackage.isNull(), dataPackage.getContent()));
		}
	}
}
