package baseSC.server;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import baseSC.data.DTO;
import baseSC.data.InputStreamHandler;
import baseSC.data.dataPackage.DataQueueIn;
import baseSC.data.events.server.ServerLostConnectionToClientEvent;
import baseSC.data.events.server.SubscribedMessageEvent;
import baseSC.data.events.server.ToServerMessageEvent;
import baseSC.data.internelDTO.HeartBeatDTO;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.packageType.PackageManager;
import baseSC.logger.CSBaseLogger;
import collection.sync.SyncManager;
import collection.sync.SyncQueue;
import config.ConfigEnvironment;
import data.queue.Action.ActionType;
import data.queue.Queue;

public class ServerClient {

	private final static int MAX_HEARTBEAT_DURATION = Integer.parseInt(ConfigEnvironment.getProperty("Connection.Max_Heartbeat_Duration"));

	private final SyncQueue<Queue<byte[]>> parsedMessages = new SyncQueue<>();
	private final SyncQueue<DTO> messages = new SyncQueue<>();

	private Socket connection;
	private BufferedOutputStream out;
	private InputStreamHandler inputHandler;
	private long id;

	protected Server server;

	private int lastPackageId;
	private long lastHeartbeat = -1;
	private boolean ended = false;
	private long ping;

	public ServerClient(Server server, Socket client, long id) {
		this.connection = client;
		this.id = id;
		this.server = server;

		try {
			connection.setTcpNoDelay(false);
			connection.setReceiveBufferSize(PackageManager.PACKAGE_DATA_SIZE);
			connection.setSendBufferSize(PackageManager.PACKAGE_DATA_SIZE);

			this.out = new BufferedOutputStream(connection.getOutputStream(), PackageManager.PACKAGE_DATA_SIZE);
			this.inputHandler = new InputStreamHandler(new DataInputStream(connection.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		new Thread(new Runnable() {
			private final SyncManager syncManager = new SyncManager();
			private final BufferedOutputStream outStream = out;

			@Override
			public void run() {
				while (isConnected()) {
					syncManager.asyncronizedWait(0, 1);

					if (!parsedMessages.isEmpty()) {
						parsedMessages.actOnContent((message) -> {
							try {
								outStream.write(message.pop());
							} catch (IOException e) {
								if (isConnected()) {
									if (!ended) {
										ended = true;
										server.getEventManager().publishServerEvent(
												new ServerLostConnectionToClientEvent(id, connection));
									}
								}
							}
							if (message.isEmpty()) {
								return ActionType.REMOVE;
							}
							return ActionType.NONE;
						});
					}
				}
			}
		}).start();

		new Thread(new Runnable() {
			private final SyncManager syncManager = new SyncManager();

			@Override
			public void run() {
				while (isConnected()) {
					syncManager.asyncronizedWait(0, 1);
					while (!messages.isEmpty()) {
						lastPackageId++;
						parsedMessages.add(server.getPackageManager().readDTO(messages.pop(), lastPackageId));
					}
				}
			}
		}).start();
	}

	public void tick() {
		checkIsAlive();
		messages.add(new HeartBeatDTO());
	}

	private void checkIsAlive() {
		if (!isConnected() || (System.currentTimeMillis() - lastHeartbeat > MAX_HEARTBEAT_DURATION && lastHeartbeat != -1)) {
			connection = null;
			if (!ended) {
				ended = true;
				server.getEventManager().publishServerEvent(new ServerLostConnectionToClientEvent(id, connection));
			}
		}
	}

	public void sendToClient(DTO message) {
		if (isConnected()) {
			messages.add(message);
		}
	}

	public void flush() {
		try {
			this.out.flush();
		} catch (IOException e) {
			if (isConnected()) {
				if (!ended) {
					ended = true;
					server.getEventManager().publishServerEvent(new ServerLostConnectionToClientEvent(id, connection));
				}
			}
		}
	}

	public void scanForIncomingData() {
		try {
			while (isConnected()) {
				DataQueueIn dataQueue = inputHandler.readData();
				CSBaseLogger.debug("new Message: " + dataQueue, id);
				if (dataQueue != null) {
					CSBaseLogger.debug("start reading Message", id);
					Object obj = server.getPackageManager().readDataQueue(dataQueue);
					CSBaseLogger.debug("read Message: " + obj, id);
					if(obj instanceof HeartBeatDTO) {
						this.lastHeartbeat = System.currentTimeMillis();
						this.ping = System.currentTimeMillis() - ((HeartBeatDTO) obj).getCurrentTime();
					} else if(obj instanceof SubscribedMessageDTO) {
						CSBaseLogger.debug("recieved subscribed-message from client: " + obj, id);
						server.getEventManager().publishServerEvent(new SubscribedMessageEvent(id, (SubscribedMessageDTO) obj));
					} else {
						CSBaseLogger.debug("recieved message from client: " + obj, id);
						server.getEventManager().publishServerEvent(new ToServerMessageEvent(id, obj));
					}
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			if (isConnected()) {
				if (!ended) {
					ended = true;
					server.getEventManager().publishServerEvent(new ServerLostConnectionToClientEvent(id, connection));
				}
			}
		}
	}

	public long getID() {
		return this.id;
	}

	public InetAddress getConnectionAdress() {
		return this.connection.getInetAddress();
	}

	public void closeConnection() {
		this.ended = true;
	}

	public boolean isConnected() {
		return connection != null && connection.isConnected() && !connection.isClosed() && !ended;
	}

	public long getPing() {
		return ping;
	}
}
