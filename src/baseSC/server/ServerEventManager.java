package baseSC.server;

import java.util.ArrayList;

import baseSC.data.events.server.NewClientConnectionEvent;
import baseSC.data.events.server.NewClientConnectionEventListener;
import baseSC.data.events.server.ServerEvent;
import baseSC.data.events.server.ServerLostConnectionToClientEvent;
import baseSC.data.events.server.ServerLostConnectionToClientEventListener;
import baseSC.data.events.server.SubscribedMessageEvent;
import baseSC.data.events.server.ToServerMessageEvent;
import baseSC.data.events.server.ToServerMessageEventListener;
import collection.sync.SyncQueue;
import data.queue.Queue;

public class ServerEventManager {

	@SuppressWarnings("unchecked")
	private ArrayList<ToServerMessageEventListener>[] serverMessageEventListenerdb = new ArrayList[10];

	@SuppressWarnings("unchecked")
	private ArrayList<NewClientConnectionEventListener>[] newClientConnectionEventListenerdb = new ArrayList[10];

	@SuppressWarnings("unchecked")
	private ArrayList<ServerLostConnectionToClientEventListener>[] serverLostConnectionToClientEventListenerdb = new ArrayList[10];

	private final SyncQueue<ServerEvent> publishedEvents = new SyncQueue<>();

	private boolean isParallelMode = false;
	
	private Server server;

	public ServerEventManager(Server server) {
		this.server = server;
	}

	public void publishServerEvent(ServerEvent event) {
		this.publishedEvents.add(event);
	}

	public void registerServerMessageEventListener(ToServerMessageEventListener listener, int priority) {
		if (priority > serverMessageEventListenerdb.length - 1) {
			priority = serverMessageEventListenerdb.length - 1;
		}
		if (this.serverMessageEventListenerdb[priority] == null) {
			this.serverMessageEventListenerdb[priority] = new ArrayList<>();
		}
		this.serverMessageEventListenerdb[priority].add(listener);
	}

	public void registerNewClientConnectionEventListener(NewClientConnectionEventListener listener, int priority) {
		if (priority > newClientConnectionEventListenerdb.length - 1) {
			priority = newClientConnectionEventListenerdb.length - 1;
		}
		if (this.newClientConnectionEventListenerdb[priority] == null) {
			this.newClientConnectionEventListenerdb[priority] = new ArrayList<>();
		}
		this.newClientConnectionEventListenerdb[priority].add(listener);
	}

	public void registerServerLostConnectionToClientEventListener(ServerLostConnectionToClientEventListener listener, int priority) {
		if (priority > serverLostConnectionToClientEventListenerdb.length - 1) {
			priority = serverLostConnectionToClientEventListenerdb.length - 1;
		}
		if (this.serverLostConnectionToClientEventListenerdb[priority] == null) {
			this.serverLostConnectionToClientEventListenerdb[priority] = new ArrayList<>();
		}
		this.serverLostConnectionToClientEventListenerdb[priority].add(listener);
	}

	public void tick() {
		Queue<ServerEvent> events = publishedEvents.clearAndClone();

		while (!events.isEmpty()) {
			ServerEvent event = events.get();
			events.remove();

			if (isParallelMode) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						handleEvent(event);
					}
				}).start();
			} else {
				try {
					handleEvent(event);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void handleEvent(ServerEvent event) {
		if (event instanceof ToServerMessageEvent || event instanceof SubscribedMessageEvent) {
			handleNewEvent(event, this.serverMessageEventListenerdb);
		} else if (event instanceof NewClientConnectionEvent) {
			handleNewEvent(event, this.newClientConnectionEventListenerdb);
		} else if (event instanceof ServerLostConnectionToClientEvent) {
			handleNewEvent(event, this.serverLostConnectionToClientEventListenerdb);
		}
	}

	private void handleNewEvent(ServerEvent event, ArrayList<?>[] eventListnerdb) {
		for (int prorityLevel = 0; prorityLevel < eventListnerdb.length && event.isActive(); prorityLevel++) {
			ArrayList<?> eventListener = eventListnerdb[prorityLevel];
			if (eventListener != null) {
				for (int i = 0; i < eventListener.size() && event.isActive(); i++) {
					Object listener = eventListener.get(i);
					if (event instanceof ToServerMessageEvent) {
						ToServerMessageEventListener tsm = (ToServerMessageEventListener) listener;
						tsm.messageFromClient((ToServerMessageEvent) event);
					} else if (event instanceof SubscribedMessageEvent) {
						ToServerMessageEventListener tsm = (ToServerMessageEventListener) listener;
						tsm.messageFromClient((SubscribedMessageEvent) event);
					} else if (event instanceof NewClientConnectionEvent) {
						NewClientConnectionEventListener ncc = (NewClientConnectionEventListener) listener;
						ncc.newServerClient((NewClientConnectionEvent) event);
					} else if (event instanceof ServerLostConnectionToClientEvent) {
						ServerLostConnectionToClientEventListener slctc = (ServerLostConnectionToClientEventListener) listener;
						slctc.connectionLost((ServerLostConnectionToClientEvent) event);
					}
				}
			}
		}
		
		if (event instanceof SubscribedMessageEvent) {
			SubscribedMessageEvent sme = (SubscribedMessageEvent) event;
			if (!sme.isActive() && !sme.doLeaveUnAnswered()) {
				server.getConnectionHandler().getServerClient(sme.getClientID()).sendToClient(
						sme.getAnswer() != null ? sme.getAnswer().copy() : sme.getRequest().copy());
			}
		}
		if (event.isActive()) {
			publishServerEvent(event);
		}
	}

	public void enableParallelMode() {
		this.isParallelMode = true;
	}

	public void disableParallelMode() {
		this.isParallelMode = false;
	}

}
