package baseSC.server;

import baseSC.data.events.server.ServerLostConnectionToClientEvent;
import baseSC.data.internelDTO.KickMessageDTO;

public class KickTask implements ServerTask{
	
	private long clientId;
	private KickMessageDTO kickMessageDTO;
	private boolean messageSend = false;
	
	public KickTask(long clientId, String reason) {
		this.clientId = clientId;

		this.kickMessageDTO = new KickMessageDTO(reason);
	}

	@Override
	public long getClientId() {
		return clientId;
	}

	@Override
	public boolean act(Server server) {
		if(!messageSend) {
			server.getConnectionHandler().getServerClient(clientId).sendToClient(kickMessageDTO);
			messageSend = true;
			return false;
		} else {
			server.getConnectionHandler().getServerClient(clientId).closeConnection();			
			server.getEventManager().publishServerEvent(new ServerLostConnectionToClientEvent(clientId, null));
			return true;
		}
	}
		

}
