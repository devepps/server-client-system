package baseSC.server;

import collection.sync.SyncQueue;
import data.queue.Queue;

public class ServerTaskManager {

	private final SyncQueue<ServerTask> tasks = new SyncQueue<>();

	private Server server;

	public ServerTaskManager(Server server) {
		super();
		this.server = server;
	}

	public void tick() {
		Queue<ServerTask> copyTasks = tasks.clearAndClone();

		while (!copyTasks.isEmpty()) {
			ServerTask currentTask = copyTasks.pop();

			boolean remove = currentTask.act(server);
			if (!remove) {
				tasks.add(currentTask);
			}
		}
	}

	public void addTask(ServerTask task) {
		tasks.add(task);
	}
}
