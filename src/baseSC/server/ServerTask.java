package baseSC.server;

public interface ServerTask {
	
	public boolean act(Server server);
	
	public long getClientId();

}
