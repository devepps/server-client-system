package baseSC.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import baseSC.data.events.server.NewClientConnectionEvent;
import baseSC.data.exceptions.server.ServerPortException;
import baseSC.data.packageType.PackageManager;
import collection.sync.SyncManager;

public class Server {

	protected int port;

	protected ServerSocket serverSocket;
	private boolean run = true;

	private ConnectionHandler connectionHandler;
	private ServerTaskManager serverTaskManager;
	private ServerEventManager eventManager;
	private PackageManager packageManager;

	public Server(int port) {
		this.port = port;
		this.serverTaskManager = new ServerTaskManager(this);
		this.connectionHandler = new ConnectionHandler(this);
		this.packageManager = new PackageManager();
		this.eventManager = new ServerEventManager(this);
	}
	
	protected void loadServer() {
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
			throw new ServerPortException(e, port);
		}		
	}

	public void openConnection() {
		loadServer();

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (run) {
					try {
						Socket client = serverSocket.accept();
						handleNewClient(client);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		new Thread(new Runnable() {
			private final SyncManager syncManager = new SyncManager();

			@Override
			public void run() {
				while (run) {
					serverTaskManager.tick();
					syncManager.asyncronizedWait(0, 1);
				}
			}
		}).start();

		new Thread(new Runnable() {
			private final SyncManager syncManager = new SyncManager();

			@Override
			public void run() {
				while (run) {
					connectionHandler.tick();
					syncManager.asyncronizedWait(1000);
				}
			}
		}).start();
	}

	protected void handleNewClient(Socket client) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				ServerClient sc = connectionHandler.registerNewConnection(client);
				eventManager.publishServerEvent(new NewClientConnectionEvent(sc.getID()));
				while (sc != null && sc.isConnected()) {
					sc.scanForIncomingData();
				}
			}
		}).start();
	}

	boolean isConnected() {
		return this.serverSocket != null && this.serverSocket.isBound();
	}

	public void tickConnection() {
		this.connectionHandler.tick();
		this.eventManager.tick();
	}

	public ConnectionHandler getConnectionHandler() {
		return connectionHandler;
	}

	public PackageManager getPackageManager() {
		return packageManager;
	}

	public ServerEventManager getEventManager() {
		return eventManager;
	}

	public ServerTaskManager getServerTaskManager() {
		return serverTaskManager;
	}
}
