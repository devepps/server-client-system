package baseSC.server;


public class ServerMessage {

	private long clientId;
	private Object objectDTO;

	public ServerMessage(long clientId, Object objectDTO) {
		super();
		this.clientId = clientId;
		this.objectDTO = objectDTO;
	}

	public long getClientId() {
		return clientId;
	}

	public Object getObjectDTO() {
		return objectDTO;
	}
}
