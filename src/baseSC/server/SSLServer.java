package baseSC.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

import baseSC.data.exceptions.server.ServerPortException;
import config.ConfigEnvironment;

public class SSLServer extends Server {

	private File keyStore;
	private String keyStorePassword;

	public SSLServer(int port, String keyStorePath, String keyStorePassword) {
		super(port);
		this.keyStore = new File(ConfigEnvironment.getProperty(keyStorePath));
		this.keyStorePassword = keyStorePassword;
	}

	public File getKeyStore() {
		return keyStore;
	}

	public String getKeyStorePassword() {
		return keyStorePassword;
	}
	
	@Override
	protected void loadServer() {
		try {
	        char[] passphrase = ConfigEnvironment.getProperty(keyStorePassword).toCharArray();
	
	        SSLContext ctx = SSLContext.getInstance("TLS");
	        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
	        KeyStore ks = KeyStore.getInstance("JKS");
	
	        ks.load(new FileInputStream(keyStore.getAbsolutePath()), passphrase);
	        kmf.init(ks, passphrase);
	        ctx.init(kmf.getKeyManagers(), null, null);
	
	        ServerSocketFactory ssocketFactory = ctx.getServerSocketFactory();
	        
			this.serverSocket = ssocketFactory.createServerSocket(port);
		} catch (IOException | NoSuchAlgorithmException | CertificateException | KeyManagementException
				| UnrecoverableKeyException | KeyStoreException e) {
			e.printStackTrace();
			throw new ServerPortException(e, port);
		}
	}

}
