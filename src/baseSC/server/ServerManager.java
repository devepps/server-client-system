package baseSC.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import baseSC.data.DTO;
import baseSC.data.exceptions.server.InvalidServerClientIDException;
import baseSC.data.exceptions.server.ServerPortException;
import baseSC.data.packageType.PackageManager;

public class ServerManager {

	private Server server;
	private boolean connectionOpen = false;

	public ServerManager(int port) {
		this.server = new Server(port);
	}

	public ServerManager(int port, String keyStorePath, String keyStorePassword) {
		this.server = new SSLServer(port, keyStorePath, keyStorePassword);
	}

	public void openConnection() throws ServerPortException {
		if (!connectionOpen) {
			this.server.openConnection();
			this.connectionOpen = true;
		}
	}

	public ServerEventManager getEventManager() {
		return this.server.getEventManager();
	}

	public void sendMessage(long clientId, DTO objectDTO) throws InvalidServerClientIDException {
		if (server.getConnectionHandler().hasClient(clientId)) {
			this.server.getConnectionHandler().getServerClient(clientId).sendToClient(objectDTO.copy());
		} else {
			throw new InvalidServerClientIDException(clientId);
		}
	}

	public boolean isConnected() {
		return connectionOpen && server.isConnected();
	}

	public boolean isConnectionOpen() {
		return connectionOpen;
	}

	public ArrayList<Long> getConnectedClients() {
		return server.getConnectionHandler().getConnectedClients();
	}

	public void addTask(ServerTask serverTask) throws InvalidServerClientIDException {
		if (server.getConnectionHandler().hasClient(serverTask.getClientId())) {
			this.server.getServerTaskManager().addTask(serverTask);
		} else {
			throw new InvalidServerClientIDException(serverTask.getClientId());
		}
	}

	public PackageManager getDataPackageManager() {
		return server.getPackageManager();
	}

	public String getClientIP(long clientId) {
		if (!server.getConnectionHandler().hasClient(clientId)) {
			throw new InvalidServerClientIDException(clientId);
		} else {
			ServerClient sc = server.getConnectionHandler().getServerClient(clientId);
			String ip = sc.getConnectionAdress().getHostAddress();
			if (ip.contains(":")) {
				ip = ip.substring(0, ip.indexOf(':'));
			}
			return ip;
		}
	}
	
	public boolean hasIP(long clientId, String ip) {
		if (!server.getConnectionHandler().hasClient(clientId)) {
			throw new InvalidServerClientIDException(clientId);
		} else {
			ServerClient sc = server.getConnectionHandler().getServerClient(clientId);
			try {
				return sc.getConnectionAdress().equals(InetAddress.getByName(ip));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return false;
			}
		}
	}

	public long getPing(long clientId) {
		if (!server.getConnectionHandler().hasClient(clientId)) {
			throw new InvalidServerClientIDException(clientId);
		} else {
			ServerClient sc = server.getConnectionHandler().getServerClient(clientId);
			return sc.getPing();
		}
	}
}
