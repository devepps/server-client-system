package baseSC.server;

import java.net.Socket;
import java.util.ArrayList;

import collection.sync.SyncArrayList;
import collection.sync.SyncHashMap;

public class ConnectionHandler {

	private final SyncHashMap<Long, ServerClient> connectedConnections = new SyncHashMap<>();
	private final SyncArrayList<Long> connections = new SyncArrayList<>();

	private long lastID = 1l;
	private Server server;

	public ConnectionHandler(Server server) {
		this.server = server;
	}

	public ServerClient registerNewConnection(Socket client) {
		Long id = generateNewClientID();
		ServerClient sc = new ServerClient(this.server, client, id);
		
		this.connections.add(id);
		this.connectedConnections.put(id, sc);
		return sc;
	}

	public long generateNewClientID() {
		lastID++;
		return lastID-1;
	}
	
	public ServerClient getServerClient(long id){
		return this.connectedConnections.get(id);
	}

	public void tick() {
		for(int i = 0; i<connections.size(); i++){
			ServerClient client = connectedConnections.get(connections.get(i));
			if(client.isConnected())client.tick();
			else{
				this.connections.remove(client.getID());
				this.connectedConnections.remove(client.getID());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Long> getConnectedClients() {
		return (ArrayList<Long>) connections.clone();
	}

	public boolean hasClient(long clientId) {
		return connections.contains(clientId);
	}
}
